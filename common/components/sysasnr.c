/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*=================================================================
 *  sysasnr.c: ASNR creation routines.
 *=================================================================*/

#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>

#include <types.h>
#include <ti/mas/util/ecomem.h>
#include <ti/mas/vpe/asnr.h>

#include "sys.h"

asnrSizeConfig_t asnrSizeConfig = {
  asnr_SRATE_16K      /* 16kHz sampling rate */
};

asnrOpenConfig_t asnrOpenConfig = {
  asnr_SRATE_16K      /* use 16kHz sampling rate */
};

/*=================================================================
 *  void sysAsnrCreate(void)     Create ASNR's within virtual mics
 *=================================================================*/
void sysAsnrCreate(void)
{
  int k, err;
  tint nbufs;
  const ecomemBuffer_t  *bufs;
  asnrNewConfig_t asnrNewConfig;
  asnrControl_t   asnrCtl;

  System_printf("...Initializing ASNR's\n");
  System_flush();

  /* Configure ASNR's (use same configuration for all) */
  err = asnrGetSizes(&nbufs, &bufs, &asnrSizeConfig);
  if (err != asnr_NOERR) {
    System_printf("*** ASNR's getsizes error: %d\n", err);
    BIOS_exit(0);
  }
  /* Allocate memory for ASNR's */
  if (nbufs > SYS_COMP_MAXBUFS) {
    System_printf("*** not enough buffer descriptors");
    BIOS_exit(0);
  }

  asnrNewConfig.sizeCfg = asnrSizeConfig;   /* Use same configuration for all */
  for (k = 0; k < sysContext.nvmics; k++) {
    err = sysHeapAllocAll(nbufs, sysCompBufs, (const void*)bufs);
    SYS_CHECK_ERROR(err);
  
    /* Give memory to ASNR #k */
    asnrNewConfig.handle = (void*)k;        /* Indicate ASNR instance #k */
    sysContext.asnrInst_p[k] = NULL;
    err = asnrNew(&sysContext.asnrInst_p[k], nbufs, sysCompBufs, &asnrNewConfig);
    if (err != asnr_NOERR) {
      System_printf("*** ASNR #%d new error: %d\n", k, err);
      BIOS_exit(0);
    }
  }

  /* Open ASNR for business */
  for (k = 0; k < sysContext.nvmics; k++) {
    err = asnrOpen(sysContext.asnrInst_p[k],&asnrOpenConfig);
    if (err != asnr_NOERR) {
      System_printf("*** ASNR #%d open error: %d\n", k, err);
      BIOS_exit(0);
    }
  }
  /* At this point ASNR's are open, but may need additional configuration */

  /* Here we reconfigure the ASNR */
  asnrCtl.valid_bitfield  = (asnr_CTL_VALID_BAND1_MAX_ATTEN |
                             asnr_CTL_VALID_BAND2_MAX_ATTEN |
                             asnr_CTL_VALID_BAND3_MAX_ATTEN);
  asnrCtl.band1_max_atten = 12;
  asnrCtl.band2_max_atten = 9;
  asnrCtl.band3_max_atten = 6;
  for (k = 0; k < sysContext.nvmics; k++) {
    err = asnrControl (sysContext.asnrInst_p[k], &asnrCtl);
    if (err != asnr_NOERR) {
      System_printf("*** ASNR #%d control error: %d\n", k, err);
      BIOS_exit(0);
    }
  }

  System_printf("Done with ASNR's\n");
  System_flush();

} /* sysAsnrCreate */

/* nothing past this point */

