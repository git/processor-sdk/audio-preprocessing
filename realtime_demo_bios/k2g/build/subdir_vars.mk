################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CMD_SRCS += \
../bflinker.cmd 

CFG_SRCS += \
../K2G_bf_rt.cfg 

C_SRCS += \
../../../common/components/fil.c \
../../../common/components/mhm/src/mhm.c \
../../../common/components/mss/src/mss.c \
../../../common/components/mss/src/mssproc.c \
../../../common/components/sys.c \
../../../common/components/sysasnr.c \
../../../common/components/sysbf.c \
../../../common/components/sysbfflt.c \
../../../common/components/sysdrc.c \
../../../common/components/syseram.c \
../../../common/components/sysiram.c \
../../../common/components/sysmss.c 

OBJS += \
./fil.obj \
./mhm.obj \
./mss.obj \
./mssproc.obj \
./sys.obj \
./sysasnr.obj \
./sysbf.obj \
./sysbfflt.obj \
./sysdrc.obj \
./syseram.obj \
./sysiram.obj \
./sysmss.obj 

C_DEPS += \
./fil.d \
./mhm.d \
./mss.d \
./mssproc.d \
./sys.d \
./sysasnr.d \
./sysbf.d \
./sysbfflt.d \
./sysdrc.d \
./syseram.d \
./sysiram.d \
./sysmss.d 

GEN_MISC_DIRS += \
./configPkg 

GEN_CMDS += \
./configPkg/linker.cmd 

GEN_OPTS += \
./configPkg/compiler.opt 

GEN_FILES += \
./configPkg/linker.cmd \
./configPkg/compiler.opt 

GEN_FILES__QUOTED += \
"configPkg\linker.cmd" \
"configPkg\compiler.opt" 

GEN_MISC_DIRS__QUOTED += \
"configPkg" 

C_DEPS__QUOTED += \
"fil.d" \
"mhm.d" \
"mss.d" \
"mssproc.d" \
"sys.d" \
"sysasnr.d" \
"sysbf.d" \
"sysbfflt.d" \
"sysdrc.d" \
"syseram.d" \
"sysiram.d" \
"sysmss.d" 

OBJS__QUOTED += \
"fil.obj" \
"mhm.obj" \
"mss.obj" \
"mssproc.obj" \
"sys.obj" \
"sysasnr.obj" \
"sysbf.obj" \
"sysbfflt.obj" \
"sysdrc.obj" \
"syseram.obj" \
"sysiram.obj" \
"sysmss.obj" 

C_SRCS__QUOTED += \
"../../../common/components/fil.c" \
"../../../common/components/mhm/src/mhm.c" \
"../../../common/components/mss/src/mss.c" \
"../../../common/components/mss/src/mssproc.c" \
"../../../common/components/sys.c" \
"../../../common/components/sysasnr.c" \
"../../../common/components/sysbf.c" \
"../../../common/components/sysbfflt.c" \
"../../../common/components/sysdrc.c" \
"../../../common/components/syseram.c" \
"../../../common/components/sysiram.c" \
"../../../common/components/sysmss.c" 


