/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      main.c
 *
 * \brief     Audio DC analog test main file
 *
 *  Audio analog test verifies the functionality of ADC and DAC available on
 *  audio daughter card. During this test, data received from ADC will be
 *  sent to DAC. There are two instances of DAC and ADC available on
 *  audio daughter card. One ADC supports 4 analog channels and one DAC
 *  supports 8 analog channels. Data received from one DAC shall be sent
 *  to both the ADCs making 8 channel Rx/Tx. Each DAC should be validated
 *  separately.
 *
 */

#include "analog_test.h"
#include "board.h"

/* System header files */
#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

/*----------------------------------------
 * BIOS header files
 *----------------------------------------*/

#include <ti/sysbios/BIOS.h>            /* mandatory - if you call APIs like BIOS_start() */

/* Portable data types */
#include <ti/mas/types/types.h>
#include <ti/mas/util/ecomem.h>

#include "../../../common/components/fil.h"
#include "../../../common/components/sys.h"

#include "ti/drv/uart/UART_stdio.h"

/* Global System Configuration Structure */
sysConfig_t sysConfig = {
  SYS_MICS_MAX,   /* #microphones */
  SYS_VMICS_MAX,  /* #virtual microphones */
  5,              /* 5ms ASNR delay */
  { 12, 9, 6},    /* ASNR band1=12dB, band2=9dB, band3=6dB */
  TRUE,           /* Enable ASNR */
  FALSE,          /* Do not use file I/O to load files (must use GEL to load into memory) */
  TRUE,           /* Use default angles for microphone configurations */
  FALSE,           /* Enable VAD */
  -50,            /* drc expansion knee point in dBm0 (dBFS is dBm0-3dB */
  6,              /* drc max gain in dB */
  FALSE           /* Disable DRC */
};

/* Global FILE I/O Configuration Structure (must be configured even when file I/O not used */
filConfig_t filConfig = {
  FIL_LENGTH_MAX,   /* number of samples to load from a file */
  SYS_MICS_MAX,     /* number of files to use */
  TRUE,             /* big endian */
  FALSE,            /* do not wrap around */
  40*SYS_FS_HZ      /* Process 40s of the signal (current mic input file length */
};

/* Acoustic environment */
/* Room Size (X,Y,Z) in meters */
/*    5.2000    4.3000    2.7000  */

/* Table size (X,Y,Z) in meters */
/*    3.0000    2.4000    0.7600  */

/* "Echo" generating speaker position (X,Y,Z) in meters */
/*    0         2.1500    1.2172     180  -90   VM-9 */

/* People positions (X,Y,Z) in meters and angles relative to the table and array  */
/*    1.8200    0.6452    1.2172  position#1  (person#1)   -123.6056 -146.3944 VM-7
      2.8200    0.6452    1.2172  -not used-                -90.0000  180.0000 VM-6
      3.8200    0.6452    1.2172              (person#2)    -56.3944  146.3944 VM-5
      1.8200    3.6548    1.2172              (person#3)    123.6056  -33.6056 VM-11
      2.8200    3.6548    1.2172  -not used-                 90.0000    0.0000 VM-0
      3.8200    3.6548    1.2172  position#6  (person#4)     56.3944   33.6056 VM-1
*/

/* Microphone array mic positions (X,Y,Z) in meters */
/*    2.7922    2.1500    0.7600  mic#1 ("left" most)    180
      2.8004    2.1304    0.7600                        -135
      2.8200    2.1222    0.7600        ("lower" most)  -90
      2.8396    2.1304    0.7600                        -45
      2.8478    2.1500    0.7600        ("right" most)   0
      2.8396    2.1696    0.7600                         45
      2.8200    2.1778    0.7600        ("upper" most)   90
      2.8004    2.1696    0.7600  mic#8                  135
*/
/* Array Center = (2.82, 2.15, 0.76) (same as table center) */

/* Noise source position (X,Y,Z) in meters */
/*    2.6000    2.1500    2.3000  */

/* Initialize FILE I/O, system context */
static void system_init(void)
{
  int err;

  err = sysCreate(&sysConfig);        /* Create system context */
  SYS_CHECK_ERROR(err);

  err = sysPrintConfig(SYSM_SCOPE_STATIC|SYSM_SCOPE_DYNAMIC);
  SYS_CHECK_ERROR(err);

} /* system_init */

/* ADC default configuration parameters */
CmbAdcConfig adcCfg =
{
	80,                  /* ADC gain */
	CMB_ADC_INL_SE_VINL1,    /* Left input mux for ADC1L */
	CMB_ADC_INL_SE_VINL3,    /* Left input mux for ADC2L */
	CMB_ADC_INR_SE_VINR1,    /* Right input mux for ADC1R */
	CMB_ADC_INR_SE_VINR3,    /* Right input mux for ADC2R */
	CMB_ADC_RX_WLEN_24BIT,   /* ADC word length */
	CMB_ADC_DATA_FORMAT_I2S, /* ADC data format */
	0
};

/* DAC default configuration parameters */
CmbDacConfig  dacCfg =
{
	CMB_DAC_AMUTE_CTRL_SCKI_LOST,   /* Amute event */
	0,                          /* Amute control */
	CMB_DAC_SAMPLING_MODE_AUTO,     /* Sampling mode */
	CMB_DAC_DATA_FORMAT_I2S,        /* Data format */
	0,                          /* Soft mute control */
	CMB_DAC_ATTENUATION_WIDE_RANGE, /* Attenuation mode */
	CMB_DAC_DEEMP_44KHZ,            /* De-emph control */
	80                          /* Volume */
};

typedef enum {
    PADCONFIG_MUX_MODE_PRIMARY = 0,  /**< Primary pin mux mode    */
    PADCONFIG_MUX_MODE_SECONDARY,    /**< Secondary pin mux mode  */
    PADCONFIG_MUX_MODE_TERTIARY,     /**< Tertiary pin mux mode   */
    PADCONFIG_MUX_MODE_QUATERNARY,   /**< Quaternary pin mux mode */
    PADCONFIG_MUX_MODE_QUINARY,      /**< Quinary pin mux mode    */
    PADCONFIG_MUX_MODE_SENARY        /**< Senary pin mux mode     */

} PadCfgMuxMode;

/*
 * Function to enable the pinmux for the mcasp and i2c devices in the soc.
 *
 */
void pinMuxSetMode_local(uint32_t padCfgId, PadCfgMuxMode muxMode)
{
	volatile uint32_t *padCfgBase;

    padCfgBase = &hBootCfg->PADCONFIG0;
    CSL_FINS(*(padCfgBase + padCfgId), BOOTCFG_PADCONFIG0_MUXMODE, muxMode);
}

void McaspDevice_init(void);

/**
 *  \brief    Initializes platform specific modules
 *
 *  This function initializes the modules like PLL, DDR, I2C etc
 *  which are required for audio processing. Need to call this function
 *  before doing any HW related operations.
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS initPlatform(void)
{
	Board_STATUS status = BOARD_SOK;
	Board_initCfg arg = BOARD_INIT_PINMUX_CONFIG | BOARD_INIT_MODULE_CLOCK | BOARD_INIT_UART_STDIO;
	status = Board_init(arg);

#if 1
	/* Configure McASP AUXCLK source as AUDIO_OSCCLK  */
	hBootCfg->SERIALPORT_CLKCTL = 0;
	/* Enable audio oscillator  */
	hBootCfg->OSC_CTL = 0x200;	//SW2:SW1 = 01 (15 - 30 MHz)

	{
		int padCfg;
		for (padCfg = 139; padCfg <= 191; padCfg++)
		{
			pinMuxSetMode_local(padCfg, PADCONFIG_MUX_MODE_QUINARY);
		}
	}
#endif

	 // I2C init
    /* Initialize all the I2C ports - Needed for different I2C devices on the board */
	evmI2CInit(I2C_PORT_0);
	evmI2CInit(I2C_PORT_1);
	evmI2CInit(I2C_PORT_2);

	///pinMuxSetMode(125, PADCONFIG_MUX_MODE_QUATERNARY);
	///gpioSetDirection(GPIO_PORT_1, 54, GPIO_OUT);
	///gpioSetOutput(GPIO_PORT_1, 54);

	/* Configure platform log messages to standard printf */
    ///Cmb_write_configure(Cmb_WRITE_UART);

    /* Initialize UART */
    ///Cmb_uart_init();
    ///Cmb_uart_set_baudrate(115200);

    return(status);
}

/**
 *  \brief    Audio analog test main function
 *
 *  \return    none
 */
void main (void)
{
	Cmb_STATUS status;

	status = initPlatform();
	if(status != Cmb_EOK)
	{
		cmb_write("Platform Init Failed!\n");
		UART_printf("Platform Init Failed!\n");
		testRet(1);
	}

	cmb_delay(10000);


	system_init();      /* initialize system context, etc. */

	sysBfCreate();      /* Create beamformers */
	sysAsnrCreate();    /* Create ASNR's */
	sysMssCreate();     /* Create MSS */
#if (SYS_USE_DRC)
	sysDrcCreate();     /* Create DRC */
#endif
	///sysVauCreate();     /* Create VAU */


	cmb_write("\n******************************************\n");
	cmb_write(  "        Audio Preprocessing Demo          \n");
    cmb_write(  "******************************************\n");

    cmb_write("\nThis Demo Takes the Audio Input from 8 Mics from CMB\n");
	cmb_write("Runs the BF+ASNR+MSS+DRC Signal Processing Chain on Mic1 through Mic7\n");
	cmb_write("The Processed Audio Output will be Sent to Left Channels of K2G EVM On-Board Line-Out\n");
	cmb_write("The Mic8 will be Sent to Right Channels of K2G EVM On-Board Line-Out\n");
	cmb_write("Please Plug a Headphone to K2G EVM On-Board Line-Out\n");

	UART_printf("\n******************************************\n");
	UART_printf(  "        Audio Preprocessing Demo          \n");
	UART_printf(  "******************************************\n");

	UART_printf("\nThis Demo Takes the Audio Input from 8 Mics from CMB\n");
	UART_printf("Runs the BF+ASNR+MSS+DRC Signal Processing Chain on Mic1 through Mic7\n");
	UART_printf("The Processed Audio Output will be Sent to Left Channels of K2G EVM On-Board Line-Out\n");
	UART_printf("The Mic8 will be Sent to Right Channels of K2G EVM On-Board Line-Out\n");
	UART_printf("Please Plug a Headphone to K2G EVM On-Board Line-Out\n");

	/* Initialize McASP HW details */
	McaspDevice_init();

	/* Configure eDMA module */
	status = eDmaConfig();
	if(status != Cmb_EOK)
	{
		cmb_write("eDMA Configuration Failed!\n");
		UART_printf("eDMA Configuration Failed!\n");
		testRet(1);
	}

	/* Initialize common audio configurations */
	status = (Cmb_STATUS)cmb_AudioInit();
	if(status != Cmb_EOK)
	{
		cmb_write("Audio Init Failed!\n");
		UART_printf("Audio Init Failed!\n");
		testRet(1);
	}

	/* reset Audio ADC module 0 */
	status = cmb_AudioAdcReset(CMB_ADC_DEVICE_0);
	cmb_delay(10000);
	/* set PLL for Audio ADC module 0 */
	status = cmb_AudioAdcSetPLL(CMB_ADC_DEVICE_0);
	cmb_delay(200000);

	/* reset Audio ADC module 1 */
	status = cmb_AudioAdcReset(CMB_ADC_DEVICE_1);
	cmb_delay(10000);

	/* Initialize Audio ADC module */
	status = audioAdcConfig(CMB_ADC_DEVICE_0, &adcCfg);
	if(status != Cmb_EOK)
	{
		cmb_write("Audio ADC0 Configuration Failed!\n");
		UART_printf("Audio ADC0 Configuration Failed!\n");
		testRet(1);
	}
	cmb_delay(10000);

	status = audioAdcConfig(CMB_ADC_DEVICE_1, &adcCfg);
	if(status != Cmb_EOK)
	{
		cmb_write("Audio ADC1 Configuration Failed!\n");
		UART_printf("Audio ADC1 Configuration Failed!\n");
		testRet(1);
	}
	cmb_delay(10000);

	/* Initialize McASP module */
	status = mcaspAudioConfig();
	if(status != Cmb_EOK)
	{
		cmb_write("McASP Configuration Failed!\n");
		UART_printf("McASP Configuration Failed!\n");
		testRet(1);
	}

	/* Start BIOS execution */
	BIOS_start();
}

/**
 *  \brief    Configures audio DAC module
 *
 *  \return    none
 */
#if CMB_AUDIO_DAC
void configAudioDAC(void)
{
	Cmb_STATUS status;

	/* Initialize Audio DAC0 module */
	status = audioDacConfig(CMB_DAC_DEVICE_0, &dacCfg);
	if(status != Cmb_EOK)
	{
		cmb_write("Audio DAC0 Configuration Failed!\n");
		UART_printf("Audio DAC0 Configuration Failed!\n");
		testRet(1);
	}
}
#endif

/* Nothing past this point */
