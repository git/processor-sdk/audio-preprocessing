/*
 * bflinker.cmd: Linker command file for Beamformer
 */

SECTIONS
{
    .bferam:        load >> IRAM
    .bfiram:        load >> IRAM
    filOutBuf0           >> EXT_RAM
	filBuf0              >> EXT_RAM
	filBuf1              >> EXT_RAM
	filBuf2              >> EXT_RAM
	filBuf3              >> EXT_RAM
	filBuf4              >> EXT_RAM
	filBuf5              >> EXT_RAM
	filBuf6              >> EXT_RAM
	filBuf7              >> EXT_RAM
	.far                 >> EXT_RAM
}
/* nothing past this point */

