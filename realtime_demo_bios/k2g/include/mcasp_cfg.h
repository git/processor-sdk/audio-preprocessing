/*
 * Copyright (c) 2015, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      mcasp_config.h
 *
 * \brief     McASP configuration header file
 *
 */

#ifndef _MCASP_CONFIG_H_
#define _MCASP_CONFIG_H_

#include "analog_test.h"

/*
 * Buffers placed in external memory are aligned on a 128 bytes boundary.
 * In addition, the buffer should be of a size multiple of 128 bytes for
 * the cache work optimally on the C6x.
 */
#define BUFLEN                  ((SYS_ADC_FS_HZ/1000)*SYS_FRAME_DURATION_MS*2)         /* Number of samples in the frame */
#define BUFALIGN                (128) /* Alignment of buffer for use of L2 cache */

/** Number of serializers configured for Rx */
#define RX_NUM_SERIALIZER       (4u)
/** Number of serializers configured for Tx */
#define TX_NUM_SERIALIZER       (1u)

/** Size of the McASP serializer buffers */
#define BUFSIZE                 (BUFLEN * sizeof(Ptr))

/** Num Bufs to be issued and reclaimed */
#define NUM_BUFS                2

/**
 *  \brief   Configures McASP module and creates the channel
 *           for audio Tx and Rx
 *
 *  \return    Cmb_EOK on Success or error code
 */
Cmb_STATUS mcaspAudioConfig(void);

#endif /* _MCASP_CONFIG_H_ */
