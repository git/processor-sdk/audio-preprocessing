1. Download AEC-AER(aer_c64Px_obj_17_0_0_0) and VOLIB(volib_C66_2_1_0_1) from http://www.ti.com/tool/telecomlib
2. Install AEC-AER(aer_c64Px_obj_17_0_0_0) and VOLIB(volib_C66_2_1_0_1) at C:\ti
3. Rename C:\ti\aer_c64Px_obj_17_0_0_0 to C:\ti\aer_c64Px_obj_17_0_0_0_mod
4. Comment out line 236~240 of C:\ti\aer_c64Px_obj_17_0_0_0_mod\packages\ti\mas\types\types.h
5. Download xdais_7_23_00_06 and xdctools_3_24_07_73 from TI website; Install them at C:\MAS_Tools
6. Copy audio-preprocessing-fw directory to C:\ti\audio-preprocessing-fw
7. Downlaod CCS 6.1.3 from TI website
8. Install CCS 6.1.3 at C:\ti
9. Launch CCS 6.1.3
10. Import AM572x_bf_nobios from C:\ti\audio-preprocessing-fw
11. Build the imported project
12. Import the target configuration file AM572x_GPEVM_BH_USB_560M.ccxml from C:\ti\audio-preprocessing-fw\file_demo\am572x\gel
13. Connect the Blackhawk USB 560M JTAG to P4 on AM572x GP EVM
14. Plug power adaptor(12V) into the AM572x GP EVM (Do not press the Power Button(S1) yet)
15. Launch the AM572x_GPEVM_BH_USB_560M.ccxml using "Launch Selected Configuration" from CCS 6.1.3
16. Press the Power Button(S1) and "Connect to CortexA15_0" immediately (Due to the EVM will be auto power off in 10 secs)
17. Then "Connect to C66xx_DSP1"
18. Load the test_rel_ce66le_CE66LE_LE_ELF.out from C:\ti\audio-preprocessing-fw\file_demo\am572x\test\ccsProjects\AM572x_bf_nobios\Debug
19. Execute GEL function Scripts-->Microphone Load Functions-->BFMicLoadAll to load all 8 microphone input files 
    (C:\ti\audio-preprocessing-fw\file_demo\am572x\gel\mics\y16L8g3m7090_x.pcm) into external memory buffers
20. Run the program (loaded in step 16) by pressing F8
21. The program will print out the statistics and "Simulation End" when the program completes the prosessing
22. Execute GEL function Scripts-->Microphone Save Functions-->BFSaveOutput to save the processed auido output from external memory buffer to a file 
    (C:\ti\audio-preprocessing-fw\file_demo\am572x\gel\mics\fileOutput.bin)
23. You can change the "#define NUM_BF_ANALOG_MIC 8" and "#define NUM_BF_VIRTUAL_MIC 12" in bf_asnr_mss_vau.h. 
    The allowed value for NUM_BF_ANALOG_MIC is 1-8. The allowed value for NUM_BF_VIRTUAL_MIC is either 12 (30 degree apart) 
    or 8 (45 degree apart)

