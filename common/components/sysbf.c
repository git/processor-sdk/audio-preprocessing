/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*=================================================================
 *  sysbf.c: BF creation routines.
 *=================================================================*/

#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>

#include <types.h>
#include <ti/mas/util/ecomem.h>
#include <ti/mas/aer/bf.h>

#include "sysbfflt.h"
#include "sys.h"

bfSizeConfig_t bfSizeConfig = {
  bf_SAMP_RATE_16K,     /* 16kHz sampling rate */
  SYS_MICS_MAX,         /* Maximum number of mics in the system */
  32,                   /* Maximum beamformer filter length */
  bf_TYPE_FIXED         /* Fixed beamformer type */
};

bfConfig_t bfConfig = {
  bf_SAMP_RATE_16K,     /* use 16kHz sampling rate */
  SYS_MICS_MAX,         /* use all mics */
};

/*=================================================================
 *  void sysBfCreate(void)     Create beamformers
 *=================================================================*/
void sysBfCreate(void)
{
  int k, n, err;
  tint nbufs;
  Fract *coeff_p;
  const ecomemBuffer_t  *bufs;
  bfNewConfig_t bfNewConfig;
  bfControl_t   bfCtl;

  System_printf("...Initializing beamformers\n");
  System_flush();

  /* Configure Beamformers (use same configuration for all) */
  err = bfGetSizes(&nbufs, &bufs, &bfSizeConfig);
  if (err != bf_NOERR) {
    System_printf("*** beamformer getsizes error: %d\n", err);
    BIOS_exit(0);
  }
  /* Allocate memory for beamformer */
  if (nbufs > SYS_COMP_MAXBUFS) {
    System_printf("*** not enough buffer descriptors");
    BIOS_exit(0);
  }

  bfNewConfig.sizeCfg = bfSizeConfig;   /* Use same configuration for all */
  for (k = 0; k < sysContext.nvmics; k++) {
    err = sysHeapAllocAll(nbufs, sysCompBufs, (const void*)bufs);
    SYS_CHECK_ERROR(err);
  
    /* Give memory to beamformer #k */
    bfNewConfig.handle = (void*)k;        /* Indicate BF instance #k */
    sysContext.bfInst_p[k] = NULL;
    err = bfNew(&sysContext.bfInst_p[k], nbufs, sysCompBufs, &bfNewConfig);
    if (err != bf_NOERR) {
      System_printf("*** beamformer #%d new error: %d\n", k, err);
      BIOS_exit(0);
    }
  }

  /* Open BF for business */
  for (k = 0; k < sysContext.nvmics; k++) {
    err = bfOpen(sysContext.bfInst_p[k],&bfConfig);
    if (err != bf_NOERR) {
      System_printf("*** beamformer #%d open error: %d\n", k, err);
      BIOS_exit(0);
    }
  }
  /* At this point BF's are open, but DISABLED! */
  /* We need to initialize filters prior to enabling them. */

  for (k = 0; k < sysContext.nvmics; k++) {
    coeff_p = sysBfFilters[sysContext.vmicangles[k]];
    for (n = 0; n < sysContext.nmics; n++) {
      err = bfPutFilter (sysContext.bfInst_p[k], coeff_p, bf_FG_BF, n, SYS_BF_FILTER_LENGTH);
      if (err != bf_NOERR) {
        System_printf("*** beamformer #%d put-filter error: %d\n", k, err);
        BIOS_exit(0);
      }
      coeff_p += SYS_BF_FILTER_LENGTH;      /* point to the filter for next mic */
    }
  }

  /* Now we can enable beamformers */
  bfCtl.valid_bitfield  = bf_CTL_CONFIG;
  bfCtl.config.mask     = bf_CTL_CFG_ENABLE;
  bfCtl.config.value    = bf_ENABLE;
  for (k = 0; k < sysContext.nvmics; k++) {
    err = bfControl (sysContext.bfInst_p[k], &bfCtl);
    if (err != bf_NOERR) {
      System_printf("*** beamformer #%d control error: %d\n", k, err);
      BIOS_exit(0);
    }
  }

  System_printf("Done with beamformers\n");
  System_flush();

} /* sysBfCreate */

/* nothing past this point */

