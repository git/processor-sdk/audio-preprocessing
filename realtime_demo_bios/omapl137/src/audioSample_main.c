/*
 * audioSample_main.c
 *
 * This file contains the test / demo code to demonstrate the Audio component 
 * driver functionality on SYS/BIOS 6.
 *
 * Copyright (C) 20017 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/** \file   audioSample_main.c
 *
 *  \brief    sample application for demonstration of audio driver usage
 *
 *  This file contains the implementation of the sample appliation for the
 *  demonstration of audio playing through the audio interface layer.
 *
 *             (C) Copyright 2017, Texas Instruments, Inc
 */

/* ========================================================================== */
/*                            INCLUDE FILES                                   */
/* ========================================================================== */

#include <xdc/std.h>
#include <string.h>
#include <xdc/runtime/Log.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/io/GIO.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <mcasp_drv.h>
#include <ti/sysbios/io/IOM.h>
#ifdef AIC_CODEC
#include <Aic31.h>
#endif
#include <ti/drv/uart/UART.h>
#include <ti/drv/uart/UART_stdio.h>

#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/csl/csl_edma3.h>
#include <ti/csl/csl_bootcfg.h>

/* Portable data types */
#include <types.h>
#include <ti/mas/util/ecomem.h>

#include "../../../common/components/fil.h"
#include "../../../common/components/sys.h"

#include "cmb.h"

/* ========================================================================== */
/*                           MACRO DEFINTIONS                                 */
/* ========================================================================== */




/* Handle to the EDMA driver instance                                         */
EDMA3_DRV_Handle hEdma;

extern void configureAudio(void);
extern void McaspDevice_init(void);
extern void configMcASP_SocHwInfo(void);

/* Global System Configuration Structure */
sysConfig_t sysConfig = {
  SYS_MICS_MAX,   /* #microphones */
  SYS_VMICS_MAX,  /* #virtual microphones */
  5,              /* 5ms ASNR delay */
  { 12, 9, 6},    /* ASNR band1=12dB, band2=9dB, band3=6dB */
  TRUE,           /* Enable ASNR */
  FALSE,          /* Do not use file I/O to load files (must use GEL to load into memory) */
  TRUE,           /* Use default angles for microphone configurations */
  FALSE,           /* Enable VAD */
  -50,            /* drc expansion knee point in dBm0 (dBFS is dBm0-3dB */
  6,              /* drc max gain in dB */
  FALSE           /* Disable DRC */
};

/* Global FILE I/O Configuration Structure (must be configured even when file I/O not used */
filConfig_t filConfig = {
  FIL_LENGTH_MAX,   /* number of samples to load from a file */
  SYS_MICS_MAX,     /* number of files to use */
  TRUE,             /* big endian */
  FALSE,            /* do not wrap around */
  40*SYS_FS_HZ      /* Process 40s of the signal (current mic input file length */
};

/* ========================================================================== */
/*                           FUNCTION DEFINITIONS                             */
/* ========================================================================== */

/* Initialize FILE I/O, system context */
static void system_init(void)
{
  int err;

  err = sysCreate(&sysConfig);        /* Create system context */
  SYS_CHECK_ERROR(err);

  err = sysPrintConfig(SYSM_SCOPE_STATIC|SYSM_SCOPE_DYNAMIC);
  SYS_CHECK_ERROR(err);

} /* system_init */

/**
 *  \brief  Void main(Void)
 *
 *   Main function of the sample application. This function enables
 *   the mcasp instance in the power sleep controller and also
 *   enables the pinmux for the mcasp instance. This also powers up
 *   any codecs if attached to McASP like the AIC codec, before switching to 
 *   the task to Audio_echo_task().
 *
 *  \param  None
 *  \return None
 */

Void main(Void)
{
    /* enable the pinmux & PSC-enable for the mcasp device    */
    configureAudio();

	system_init();      /* initialize system context, etc. */

	sysBfCreate();      /* Create beamformers */
	sysAsnrCreate();    /* Create ASNR's */
	sysMssCreate();     /* Create MSS */
#if (SYS_USE_DRC)
	sysDrcCreate();     /* Create DRC */
#endif

	cmb_write("\n******************************************\n");
	cmb_write(  "        Audio Preprocessing Demo          \n");
    cmb_write(  "******************************************\n");

    cmb_write("\nThis Demo Takes the Audio Input from 8 Mics from CMB\n");
	cmb_write("Runs the BF+ASNR+MSS+DRC Signal Processing Chain on Mic1 through Mic7\n");
	cmb_write("The Processed Audio Output will be Sent to Left Channels of OMAPL137 EVM On-Board Line-Out\n");
	cmb_write("The Mic8 will be Sent to Right Channels of OMAPL137 EVM On-Board Line-Out\n");
	cmb_write("Please Plug a Headphone to OMAPL137 EVM On-Board Line-Out\n");

	UART_printf("\n******************************************\n");
	UART_printf(  "        Audio Preprocessing Demo          \n");
	UART_printf(  "******************************************\n");

	UART_printf("\nThis Demo Takes the Audio Input from 8 Mics from CMB\n");
	UART_printf("Runs the BF+ASNR+MSS+DRC Signal Processing Chain on Mic1 through Mic7\n");
	UART_printf("The Processed Audio Output will be Sent to Left Channels of OMAPL137 EVM On-Board Line-Out\n");
	UART_printf("The Mic8 will be Sent to Right Channels of OMAPL137 EVM On-Board Line-Out\n");
	UART_printf("Please Plug a Headphone to OMAPL137 EVM On-Board Line-Out\n");

	/* Initializing McASP HwInfo parameters */
    McaspDevice_init();

    /* Perform SOC specific McASP HwInfo Configuration for non-default parameters
     * using the socGetConfig() and socSetConfig(). Please note that
      this is being called AFTER McaspDevice_init() which initializes with the
      default parameters */
    configMcASP_SocHwInfo();
#if defined(AIC_CODEC)
    Aic31_init();
#endif

    BIOS_start();

    return;
}
/*
 * Mcasp init function called when creating the driver.
 */




/* ========================================================================== */
/*                                END OF FILE                                 */
/* ========================================================================== */
