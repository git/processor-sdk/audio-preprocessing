
-c  /* ROM autoinitialization module */
-a  /* LINK USING C CONVENTIONS      */

-l ../../vpe/lib/vpe_c.ae64P
-l ../../vpe/lib/vpe_a.ae64P
-l ../../sdk/lib/sdk_c.ae64P
-l ../../util/lib/util_c.ae64P
-l ../../util/lib/util_a.ae64P

-stack 0x2000
-heap  0x5000

MEMORY
{
  IVECS   : origin = 0x800000, length = 0x000220
  L2_SRAM : origin = 0x801000, length = 0x1F0000 /* 2M - 64k */
  DDR: origin = 0xE0000000, length = 0x10000000 /* 256M external */
}

SECTIONS
{
  .text   > L2_SRAM
  .cinit  > L2_SRAM
  .switch > L2_SRAM
 
  .bss    > L2_SRAM
  .const  > L2_SRAM
  .sysmem > L2_SRAM
  .stack  > L2_SRAM
  .data   > L2_SRAM
  .cio    > L2_SRAM
  .far    > L2_SRAM

  .fardata   > L2_SRAM
  .neardata  > L2_SRAM
  .rodata    > L2_SRAM

  /* Heap used by simulation */
  .simHeap > L2_SRAM
  /* debug buffer */
  VPEExtDbg > DDR
}
 
