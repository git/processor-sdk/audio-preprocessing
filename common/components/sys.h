/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* 
  sys.h: System constants, data types, prototypes.
*/
#ifndef _SYS_H
#define _SYS_H

#include <xdc/std.h>                /* required for types.h */
#include <types.h>
#include <ti/mas/util/ecomem.h>

/*======================================================================
 * Static system configuration parameters
 *======================================================================*/

#define SYS_MICS_MAX          7       /* Maximum number of microphones in the system */
#define SYS_VMICS_MAX         12      /* Maximum number of virtual microphones in the system */
#define SYS_ADC_FS_HZ         16000   /* ADC sampling rate in Hz */
#define SYS_FS_HZ             16000   /* Sampling rate in Hz */
#define SYS_FS_RATIO          SYS_ADC_FS_HZ/SYS_FS_HZ   /* Sampling rate ratio */
#define SYS_FRAME_DURATION_MS 10      /* Frame duration in ms */
#define SYS_FRAME_LENGTH      (1L*SYS_FS_HZ*SYS_FRAME_DURATION_MS/1000)             /* Frame length in samples */
#define SYS_FRAME_SIZE        (TYP_LINSAMPLE_SIZE*SYS_FRAME_LENGTH/TYP_TWORD_SIZE)  /* Frame size in bytes */
#define SYS_IN_LENGTH         (2L*SYS_FRAME_LENGTH*SYS_MICS_MAX)                    /* Input buffer length (dual) */
#define SYS_IN_SIZE           (TYP_LINSAMPLE_SIZE*SYS_IN_LENGTH/TYP_TWORD_SIZE)     /* # of words in input buffer */
#define SYS_USE_DRC           1     												/* Do we want use DRC */

/*======================================================================
 * Bit masks, bit-mask handling macros, error codes
 *======================================================================*/

/* Bit masks for sysPrintConfig */
#define SYSM_SCOPE_STATIC   0x1     /* Static system information */
#define SYSM_SCOPE_DYNAMIC  0x2     /* Dynamic system onfirmation */

/* Macros for bit-mask manipulations */
#define SYS_MASK_GET(value,mask)    ((value)&(mask))
#define SYS_MASK_SET(value,mask)    ((value)|(mask))
#define SYS_MASK_TEST(value,mask)   (SYS_MASK_GET(value,mask)!=0)

/* System error codes */
#define SYS_ERR_SUCCESS       0
#define SYS_ERR_BADCONFIG     (-1)
#define SYS_INV_HANDLE        (-2)
#define SYS_INV_LOGIC         (-3)
#define SYS_ERR_BUFLENGTH     (-4)
#define SYS_ERR_FOPEN         (-5)
#define SYS_ERR_FLOAD         (-6)
#define SYS_ERR_FMAXBUF       (-7)
#define SYS_ERR_FREAD         (-8)
#define SYS_ERR_HEAPINIT      (-9)
#define SYS_ERR_INVHEAPCLASS  (-10)
#define SYS_ERR_BFERROR       (-11)
#define SYS_ERR_EOF           (-12)
#define SYS_ERR_ASNRERROR     (-13)
#define SYS_ERR_MSSERROR      (-14)
#define SYS_ERR_MSSDEBUG      (-15)
#define SYS_ERR_ANGLECONFIG   (-16)
#define SYS_ERR_VAUERROR      (-17)
#define SYS_ERR_NOMEMORY      (-18)

/*======================================================================
 * System Data Types
 *======================================================================*/

/* System Configuration Structure */
struct sysConfig_stc {
  tint  nmics;        /* The actual number of microphones in ths system */
  tint  nvmics;       /* The acutal number of virtual microphones in the system */
  tint  asnr_delay;   /* delay in ms. Default is 5ms. */
  tint  asnr_attn[3]; /* band1, band2, band3 attenutation for ASNR (dB) */
  tint  asnr_enable;  /* TRUE: enable ASNR, FALSE: disable it */
  tint  use_fileio;   /* Use file I/O to load microphone files */
  tint  use_default;  /* Use default angles for virtual microphones */
  tint  vad_enable;   /* TRUE: enable VAD, FALSE: disable it */
  tint  drc_exp_knee; /* in dBm0! (default was -50dBm0 */
  tint  drc_max_amp;  /* max gain in dB (default was 6dB */
  tint  drc_enable;   /* TRUE: enable DRC, FALSE: disable it */
};
typedef struct sysConfig_stc sysConfig_t;

/* System Context Structure */
struct sysContext_stc {
  tint  nmics;    /* The actual number of microphones in ths system */
  tint  nvmics;   /* The acutal number of virtual microphones in the system */

  /* heap handles (point to head header) */
  void *heapEP;
  void *heapES;
  void *heapIP;
  void *heapIS;

  /* used heap */
  tulong heapEP_used;     /* how many used in words */
  tulong heapES_used;
  tulong heapIP_used;
  tulong heapIS_used;

  /* Mic input buffer */
  /* This is a dual buffer. Each part has SYS_FRAME_LENGTH*SYS_MICS_MAX samples
   * to accomodate all microphone inputs. The samples are written contiguously
   * without any gaps. Channel after channel.
   */
  void *in_w;     /* Mic input buffer being written to */
  void *in_r;     /* Mic input buffer being read from */
  void *in_lo;    /* Low part of mic input buffer */
  void *in_hi;    /* High part of mic input buffer */

  linSample *vmicfrm[SYS_VMICS_MAX];  /* Virtual mic frames */

  /* Instance pointers */
  void *bfInst_p[SYS_VMICS_MAX];      /* beamformer instance pointers */
  void *asnrInst_p[SYS_VMICS_MAX];    /* ASNR instance pointers */
  void *mssInst_p;                    /* MSS instance pointer */
  void *vauInst_p;                    /* VAU instance pointer */
  void *drcInst_p;                    /* DRC instance pointer */

  tint vmicangles[SYS_VMICS_MAX];     /* use SYS_BF_ANGLE_xxx from sysbfflt.h */

  tint  asnr_delay;   /* in ms. Default is 5ms. */
  tint  asnr_attn[3]; /* band 1,2,3 attenutations in dB */
  tint  asnr_enable;  /* TRUE: enable, FALSE: disable ASNR */
  tint  use_fileio;   /* Use file I/O to load microphone files */
  tint  eof;          /* End of file reached */
  tint  use_default;  /* Use default vmic angles (4,6,8,12 vmics supported only) */
  tint  vad_enable;   /* TRUE: enable VAD, FALSE: disable it */
  tint  drc_exp_knee; /* in dBm0 (default was -50dBm0 */
  tint  drc_max_amp;  /* max gain in dB (default was 6dB */
  tint  drc_enable;   /* TRUE: enable DRC, FALSE: disable it */
};
typedef struct sysContext_stc  sysContext_t;

/*======================================================================
 * Global variable declarations
 *======================================================================*/

extern sysContext_t sysContext;         /* Defined in sys.c */
extern tint         sysBfVMicAngles[];  /* Defined in sys.c */

/* Buffer descriptor array for memory allocations */
#define SYS_COMP_MAXBUFS    12
extern ecomemBuffer_t sysCompBufs[];

/*======================================================================
 * Additional Macros
 *======================================================================*/

/* Misc macros */
#define SYS_CHECK_ERROR(err)    if((err)<0){sysError(err);}

/*======================================================================
 * API Prototypes
 *======================================================================*/

/* sys.c */
extern int  sysCreate(sysConfig_t*);              /* Initialize system context */
extern void sysError(int);                        /* Printf error code and exit */
extern int  sysHeapAlloc(void*, tint);            /* Allocate from appropriate heap */
extern int  sysHeapAllocAll(tint, void*, const void*);  /* Allocate ALL buffers */
extern int  sysPrintConfig(tuint);                /* Print system configuration */

/* sysbf.c */
extern void sysBfCreate(void);        /* Create all active beamformers */

/* sysasnr.c */
extern void sysAsnrCreate(void);      /* Create all active ASNR's */

/* sysmss.c */
extern void sysMssCreate(void);       /* Create the MSS module */

/* sysdrc.c */
extern void sysDrcCreate(void);       /* Create the DRC module */

/* sysvau.c */
extern void sysVauCreate(void);       /* Create the VAU module */

#endif
/* nothing past this point */

