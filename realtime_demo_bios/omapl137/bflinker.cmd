/*
 * bflinker.cmd: Linker command file for Beamformer
 */

SECTIONS
{
    .bferam:        load >> IRAM
    .bfiram:        load >> IRAM
    .uiaLoggerBuffers load >> SDRAM
    filOutBuf0           >> SDRAM
	filBuf0              >> SDRAM
	filBuf1              >> SDRAM
	filBuf2              >> SDRAM
	filBuf3              >> SDRAM
	filBuf4              >> SDRAM
	filBuf5              >> SDRAM
	filBuf6              >> SDRAM
	filBuf7              >> SDRAM
	.far                 >> SDRAM
}
/* nothing past this point */

