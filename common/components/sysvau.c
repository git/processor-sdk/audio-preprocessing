/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*=================================================================
 *  sysvau.c: VAU creation routines.
 *=================================================================*/

#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>

#include <types.h>
#include <ti/mas/util/ecomem.h>
#include <ti/mas/vau/vau.h>

#include "sys.h"

vauSizeConfig_t vauSizeConfig = {
  SYS_FRAME_LENGTH,     /* 10ms frames (160 samples) */
  vau_SAMP_RATE_16KHZ   /* 16kHz sampling rate */
};

vauOpenConfig_t vauOpenConfig = {
  SYS_FRAME_LENGTH,     /* use 160 sample frames */
  vau_SAMP_RATE_16KHZ,  /* use 16kHz sampling rate */
  { 0, 0, 0, 0, 0}      /* Do not reconfigure defaults */
};

/*=================================================================
 *  void sysVauCreate(void)     Create VAU instance
 *=================================================================*/
void sysVauCreate(void)
{
  int err;
  tint nbufs;
  const ecomemBuffer_t  *bufs;
  ifvauControl_t    vauCtl;

  System_printf("...Initializing VAU\n");
  System_flush();

  /* Configure VAU */
  err = vauGetSizes(&nbufs, &bufs, &vauSizeConfig);
  if (err != vau_NOERR) {
    System_printf("*** VAU getsizes error: %d\n", err);
    BIOS_exit(0);
  }
  /* Allocate memory for VAU */
  if (nbufs > SYS_COMP_MAXBUFS) {
    System_printf("*** not enough buffer descriptors");
    BIOS_exit(0);
  }

  err = sysHeapAllocAll(nbufs, sysCompBufs, (const void*)bufs);
  SYS_CHECK_ERROR(err);
  
  /* Give memory to VAU */
  sysContext.vauInst_p = NULL;
  err = vauNew(&sysContext.vauInst_p, nbufs, sysCompBufs);
  if (err != vau_NOERR) {
    System_printf("*** VAU #%d new error: %d\n", err);
    BIOS_exit(0);
  }

  /* Open VAU for business */
  err = vauOpen(sysContext.vauInst_p,&vauOpenConfig);
  if (err != vau_NOERR) {
    System_printf("*** VAU open error: %d\n", err);
    BIOS_exit(0);
  }
  /* At this point VAU is open, but may need additional configuration */

  /* Here we enable VAU */
  vauCtl.valid_params = ifvau_VALID_CONTROL;
  vauCtl.control      = ifvau_ENABLE;
  err = vauControl (sysContext.vauInst_p, NULL, &vauCtl);
  if (err != vau_NOERR) {
    System_printf("*** VAU control error: %d\n", err);
    BIOS_exit(0);
  }

  System_printf("Done with VAU\n");
  System_flush();

} /* sysVauCreate */

/* nothing past this point */

