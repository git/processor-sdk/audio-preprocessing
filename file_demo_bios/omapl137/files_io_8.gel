/******************************************************************************
 * Copyright (c) 2012-16 Texas Instruments Incorporated - http://www.ti.com
 * 
 *  Redistribution and use in source and binary forms, with or without 
 *  modification, are permitted provided that the following conditions 
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright 
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the 
 *    documentation and/or other materials provided with the   
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated  the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 *****************************************************************************
 *
 *  Filename: file_io.gel    
 *    Description:  GEL file to load microphone files 
 *                  EVM. This GEL has functions that handle the Main PLL 
 *                  initialization. 
 *                        
 *                  More functions may be added as needed at a later date.
 *        
 *      Author: Nitin Sakhuja                  
 *      Revision History: 
 *
 *    Rev 1.0 - 02/06/2012
 *    -     Initial file creation
 ---------------------------------------------------------------------------*/

#define MIC1PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_1.pcm"
#define MIC2PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_2.pcm"
#define MIC3PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_3.pcm"
#define MIC4PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_4.pcm"
#define MIC5PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_5.pcm"
#define MIC6PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_6.pcm"
#define MIC7PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_7.pcm"
#define MIC8PATH  "$(GEL_file_dir)/../../common/t8/y16L8g3m7090_8.pcm"

#define OUTPUTPATH  "$(GEL_file_dir)/../../common/t8/fileOutput8.bin"

BFLoadMic(buffer,micpath)
{
    GEL_MemoryLoad(buffer,0,160,micpath,32);
}

BFSaveMic(buffer,num, micpath)
{
    GEL_MemorySave(buffer,0,num,micpath,8,0,32);
}

menuitem "Microphone Load Functions";

dialog BFLoadOneMic(buffer "Buffer Address",micpath "Microphone File Path")
{
    GEL_TextOut("Loading %s\n","",,,,micpath);
    BFLoadMic(buffer,micpath);
}

hotmenu BFLoadMic1()
{
    GEL_TextOut("Loading %s\n","",,,,MIC1PATH);
    BFLoadMic(filBuf0,MIC1PATH);
}

hotmenu BFLoadMic2()
{
    GEL_TextOut("Loading %s\n","",,,,MIC2PATH);
    BFLoadMic(filBuf1,MIC2PATH);
}

hotmenu BFLoadMic3()
{
    GEL_TextOut("Loading %s\n","",,,,MIC3PATH);
    BFLoadMic(filBuf2,MIC3PATH);
}

hotmenu BFLoadMic4()
{
    GEL_TextOut("Loading %s\n","",,,,MIC4PATH);
    BFLoadMic(filBuf3,MIC4PATH);
}

hotmenu BFLoadMic5()
{
    GEL_TextOut("Loading %s\n","",,,,MIC5PATH);
    BFLoadMic(filBuf4,MIC5PATH);
}

hotmenu BFLoadMic6()
{
    GEL_TextOut("Loading %s\n","",,,,MIC6PATH);
    BFLoadMic(filBuf5,MIC6PATH);
}

hotmenu BFLoadMic7()
{
    GEL_TextOut("Loading %s\n","",,,,MIC7PATH);
    BFLoadMic(filBuf6,MIC7PATH);
}

hotmenu BFLoadMic8()
{
    GEL_TextOut("Loading %s\n","",,,,MIC8PATH);
    BFLoadMic(filBuf7,MIC8PATH);
}

hotmenu BFLoadMicAll()
{
    BFLoadMic1();
    BFLoadMic2();
    BFLoadMic3();
    BFLoadMic4();
    BFLoadMic5();
    BFLoadMic6();
    BFLoadMic7();
    BFLoadMic8();
    GEL_TextOut("Done Loading Microphones\n");
}

menuitem "Microphone Save Functions";

hotmenu BFSaveOutput()
{
    GEL_TextOut("Saving %s\n","",,,,OUTPUTPATH);
    BFSaveMic(filOutBuf0,filConfig.outlen/2,OUTPUTPATH);
}
