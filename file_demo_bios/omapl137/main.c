/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*
 *  ======== main.c ========
 */


/* System header files */
#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

/*----------------------------------------
 * BIOS header files
 *----------------------------------------*/

#include <ti/sysbios/BIOS.h>            /* mandatory - if you call APIs like BIOS_start() */

/* Portable data types */
#include <types.h>
#include <ti/mas/util/ecomem.h>

#include "../../common/components/fil.h"
#include "../../common/components/sys.h"

/* Global System Configuration Structure */
sysConfig_t sysConfig = {
  SYS_MICS_MAX,   /* #microphones */
  SYS_VMICS_MAX,  /* #virtual microphones */
  5,              /* 5ms ASNR delay */
  { 12, 9, 6},    /* ASNR band1=12dB, band2=9dB, band3=6dB */
  TRUE,           /* Enable ASNR */
  FALSE,          /* Do not use file I/O to load files (must use GEL to load into memory) */
  TRUE,           /* Use default angles for microphone configurations */
  FALSE,           /* Enable VAD */
  -50,            /* drc expansion knee point in dBm0 (dBFS is dBm0-3dB */
  6,              /* drc max gain in dB */
  FALSE           /* Disable DRC */
};

/* Global FILE I/O Configuration Structure (must be configured even when file I/O not used */
filConfig_t filConfig = {
  FIL_LENGTH_MAX,   /* number of samples to load from a file */
  SYS_MICS_MAX,     /* number of files to use */
  TRUE,             /* big endian */
  FALSE,            /* do not wrap around */
  40*SYS_FS_HZ      /* Process 40s of the signal (current mic input file length */
};

/* Miocrophone input files names (in case we want to use FILE I/O to load into memory */
char *filNames[FIL_MAX] = {
#if FIL_MAX==8
  "../../../common/t8/y16L8g3m7090_1.pcm",
  "../../../common/t8/y16L8g3m7090_2.pcm",
  "../../../common/t8/y16L8g3m7090_3.pcm",
  "../../../common/t8/y16L8g3m7090_4.pcm",
  "../../../common/t8/y16L8g3m7090_5.pcm",
  "../../../common/t8/y16L8g3m7090_6.pcm",
  "../../../common/t8/y16L8g3m7090_7.pcm",
  "../../../common/t8/y16L8g3m7090_8.pcm"
#else
"../../../common/t8/y16L7g3m7090_1.pcm",
"../../../common/t8/y16L7g3m7090_2.pcm",
"../../../common/t8/y16L7g3m7090_3.pcm",
"../../../common/t8/y16L7g3m7090_4.pcm",
"../../../common/t8/y16L7g3m7090_5.pcm",
"../../../common/t8/y16L7g3m7090_6.pcm",
"../../../common/t8/y16L7g3m7090_7.pcm",
#endif
};

/* Acoustic environment */
/* Room Size (X,Y,Z) in meters */
/*    5.2000    4.3000    2.7000  */

/* Table size (X,Y,Z) in meters */
/*    3.0000    2.4000    0.7600  */

/* "Echo" generating speaker position (X,Y,Z) in meters */
/*    0         2.1500    1.2172     180  -90   VM-9 */

/* People positions (X,Y,Z) in meters and angles relative to the table and array  */
/*    1.8200    0.6452    1.2172  position#1  (person#1)   -123.6056 -146.3944 VM-7
      2.8200    0.6452    1.2172  -not used-                -90.0000  180.0000 VM-6
      3.8200    0.6452    1.2172              (person#2)    -56.3944  146.3944 VM-5
      1.8200    3.6548    1.2172              (person#3)    123.6056  -33.6056 VM-11
      2.8200    3.6548    1.2172  -not used-                 90.0000    0.0000 VM-0
      3.8200    3.6548    1.2172  position#6  (person#4)     56.3944   33.6056 VM-1
*/

/* Microphone array mic positions (X,Y,Z) in meters */
/*    2.7922    2.1500    0.7600  mic#1 ("left" most)    180
      2.8004    2.1304    0.7600                        -135
      2.8200    2.1222    0.7600        ("lower" most)  -90
      2.8396    2.1304    0.7600                        -45
      2.8478    2.1500    0.7600        ("right" most)   0
      2.8396    2.1696    0.7600                         45
      2.8200    2.1778    0.7600        ("upper" most)   90
      2.8004    2.1696    0.7600  mic#8                  135
*/
/* Array Center = (2.82, 2.15, 0.76) (same as table center) */

/* Noise source position (X,Y,Z) in meters */
/*    2.6000    2.1500    2.3000  */

/* Initialize FILE I/O, system context */
static void system_init(void)
{
  int k, err;
  tlong n;
  void *fid;

  fid = (void*)0xBABA;                /* Just to test if we're providing fid correctly */
  err = filCreate(&fid, &filConfig);  /* Create FILE I/O context */
  SYS_CHECK_ERROR(err);

  err = sysCreate(&sysConfig);        /* Create system context */
  SYS_CHECK_ERROR(err);

  err = sysPrintConfig(SYSM_SCOPE_STATIC|SYSM_SCOPE_DYNAMIC);
  SYS_CHECK_ERROR(err);

  if (sysContext.use_fileio) {        /* In case we want to read directly from file (slow) */
    for (k = 0; k < filConfig.nfiles; k++) {
      n = filLoad(filNames[k], filConfig.length, k);
      System_printf("Read %ld samples from %s\n", n, filNames[k]);
    }
  }
  System_flush();
  /* If we are not using FILE I/O to load mic files, we need to use GEL to load into memory. */

} /* system_init */

/*
 *  =============== main ==============
 */
int main(void)
{
  system_init();      /* initialize system context, etc. */

  sysBfCreate();      /* Create beamformers */
  sysAsnrCreate();    /* Create ASNR's */
  sysMssCreate();     /* Create MSS */
#if (SYS_USE_DRC)
	sysDrcCreate();     /* Create DRC */
#endif
  //sysVauCreate();     /* Create VAU */

  BIOS_start();

  return(0);          /* Never to return here */
} /* main */

/* nothing past this point */

