1. Download AEC-AER(aer_c64Px_obj_17_0_0_0) and VOLIB(volib_C64P_2_1_0_1) from http://www.ti.com/tool/telecomlib
2. Rename C:\ti\aer_c64Px_obj_17_0_0_0 to C:\ti\aer_c64Px_obj_17_0_0_0_mod
3. Comment out line 236~240 of C:\ti\aer_c64Px_obj_17_0_0_0_mod\packages\ti\mas\types\types.h
4. Install AEC-AER(aer_c64Px_obj_17_0_0_0) and VOLIB(volib_C64P_2_1_0_1) at C:\ti
5. Copy audio-preprocessing-fw directory to C:\ti\audio-preprocessing-fw
6. Download and install bios_6_45_01_29
7. Download and install xdais_7_24_00_04 and xdctools_3_32_00_06_core from TI website 
8. Downlaod CCS 6.1.3 from TI website
9. Install CCS 6.1.3 at C:\ti
10. Launch CCS 6.1.3
11. Import DA830_bf from C:\ti\audio-preprocessing-fw\file_demo_bios\da830
12. Build the imported project
13. Import the target configuration file DA830_DSK_USB560M.ccxml from C:\ti\audio-preprocessing-fw\file_demo_bios\da830
14. Connect the Blackhawk USB 560M JTAG to J4 on DA830 DSK
15. Power on the DA830DSK
16. Connect to the DA830 DSK via JTAG using CCS 6.1.3
17. Load the DA830_bf.out from C:\ti\audio-preprocessing-fw\file_demo_bios\da830\Debug
18. Execute GEL function Scripts-->Microphone Load Functions-->BFMicLoadAll to load all 8 microphone input files 
    (C:\ti\audio-preprocessing-fw\file_demo_bios\da830\t8\y16L8g3m7090_x.pcm) into external memory buffers
19. Run the program (loaded in step 13) by pressing F8
20. The program will print out the statistics and "Simulation End" when the program completes the prosessing
21. Execute GEL function Scripts-->Microphone Save Functions-->BFSaveOutput to save the processed auido output from external memory buffer to a file 
    (C:\ti\audio-preprocessing-fw\file_demo_bios\da830\t8\fileOutput.bin)
22. By default 12 virtual microphones (30 degree apart) will be used

