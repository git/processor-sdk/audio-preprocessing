/*-----------------------------------------------------------------
 * Filename:  bf_asnr_mss_vau.c
 *                            
 * Description: Demo for the BF, ASNR, MSS and VAU API functions.
 * 
 *        Copyright (c) 2016 Texas Instruments Incorporated
 *                                                                                
 *              All rights reserved not granted herein.                           
 *                                                                                
 *                         Limited License.                                       
 *                                                                                
 *  Texas Instruments Incorporated grants a world-wide, royalty-free,             
 *  non-exclusive license under copyrights and patents it now or hereafter owns   
 *  or controls to make, have made, use, import, offer to sell and sell           
 *  ("Utilize") this software subject to the terms herein.  With respect to the   
 *  foregoing patent license, such license is granted solely to the extent that   
 *  any such patent is necessary to Utilize the software alone.  The patent       
 *  license shall not apply to any combinations which include this software,      
 *  other than combinations with devices manufactured by or for TI (�TI           
 *  Devices�).  No hardware patent is licensed hereunder.                         
 *                                                                                
 *  Redistributions must preserve existing copyright notices and reproduce this   
 *  license (including the above copyright notice and the disclaimer and (if      
 *  applicable) source code license limitations below) in the documentation       
 *  and/or other materials provided with the distribution                         
 *                                                                                
 *  Redistribution and use in binary form, without modification, are permitted    
 *  provided that the following conditions are met:                               
 *                                                                                
 *    *  No reverse engineering, decompilation, or disassembly of this software   
 *  is permitted with respect to any software provided in binary form.            
 *                                                                                
 *    *  any redistribution and use are licensed by TI for use only with TI       
 *  Devices.                                                                      
 *                                                                                
 *    *  Nothing shall obligate TI to provide you with source code for the        
 *  software licensed and provided to you in object code.                         
 *                                                                                
 *  If software source code is provided to you, modification and redistribution   
 *  of the source code are permitted provided that the following conditions are   
 *  met:                                                                          
 *                                                                                
 *    *  any redistribution and use of the source code, including any resulting   
 *  derivative works, are licensed by TI for use only with TI Devices.            
 *                                                                                
 *    *  any redistribution and use of any object code compiled from the source   
 *  code and any resulting derivative works, are licensed by TI for use only      
 *  with TI Devices.                                                              
 *                                                                                
 *  Neither the name of Texas Instruments Incorporated nor the names of its       
 *  suppliers may be used to endorse or promote products derived from this        
 *  software without specific prior written permission.                           
 *                                                                                
 *  DISCLAIMER.                                                                   
 *                                                                                
 *  THIS SOFTWARE IS PROVIDED BY TI AND TI�S LICENSORS "AS IS" AND ANY EXPRESS    
 *  OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED             
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE        
 *  DISCLAIMED. IN NO EVENT SHALL TI AND TI�S LICENSORS BE LIABLE FOR ANY         
 *  DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES    
 *  (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR            
 *  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER    
 *  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT            
 *  LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY     
 *  OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH   
 *  DAMAGE.                                                                       
 *-----------------------------------------------------------------*/

/* ANSI C headers */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/* RTSC headers */
#include <ti/mas/types/types.h>
#include <ti/mas/fract/fract.h>
#include <ti/mas/types/const.h>
#include <ti/mas/util/ecomem.h>

/* for signal generator */
#include <ti/mas/sdk/sgn.h>

#include <ti/mas/noise-reduction/test/src/bf_asnr_mss_vau.h>
#include "ti/mas/noise-reduction/test/src/sysbfflt.h"
#include "ti/mas/noise-reduction/test/src/siubf.h"
#include "ti/mas/noise-reduction/test/src/siuloc.h"
#include "ti/mas/noise-reduction/test/src/siuport.h"

#if _VPESIM_C64||_VPESIM_C64_BIG_ENDIAN||_VPESIM_C64P||_VPESIM_C64P_BIG_ENDIAN |_VPESIM_C66||_VPESIM_C66_BIG_ENDIAN
#include <ti/mas/sdk/memArchCfg.h>
#endif

/* Global Varibles */

/* Global PCM data buffers */
linSample sigInBuffer[NUM_BF_VIRTUAL_MIC][vpe_SIM_MAX_SAMPLES];
linSample sigOutBuffer[vpe_SIM_MAX_SAMPLES];
linSample sigOutBufferMss[vpe_SIM_MAX_SAMPLES];
linSample *buf_Inptr[NUM_BF_VIRTUAL_MIC];
linSample *buf_Outptr;
linSample *buf_OutptrMss;
linSample *buf_Outptr2;

/* varibles for BF */
tint fltIdx[2][16] = {
  {0, 1, 3, 4, 5, 7, 8, 9, 11, 12, 13, 15, 0, 0, 0, 0}, /* 12 virtual mics 30 degree apart */
  {0, 2, 4, 6, 8, 10, 12, 0, 0, 0, 0, 0, 0, 0, 0},      /* 8 virtual mics 45 degree apart */
};
void  *bfInst[NUM_BF_VIRTUAL_MIC];
tword bfIntMem[bf_INT_HEAP_SIZE];
tword bfExtMem[bf_EXT_HEAP_SIZE];
bfMemHeap_t bfIntHeap;
bfMemHeap_t bfExtHeap;
tint num_bufs_req_by_bf;
const ecomemBuffer_t *bufs_req_by_bf;
ecomemBuffer_t bufs_aloc_for_bf[MAX_NUM_BF_BUFFS];
bfNewConfig_t bf_new_cfg;
linSample bfInFrame[NUM_BF_ANALOG_MIC][vpe_SIM_MAX_SAMPLES];
linSample *inFramePtr[NUM_BF_ANALOG_MIC];
#define bf_MIC_INPUT_SAMP_SIZE AUDIO_FILE_IN_SEC*AUDIO_SMAPLE_FREQ
#define bf_MIC_INPUT_SAMP_PAD AUDIO_SMAPLE_FREQ
#pragma DATA_SECTION (fileInBufs, ".fileInBufs")
#pragma DATA_ALIGN(fileInBufs, 8)
linSample fileInBufs[NUM_BF_ANALOG_MIC][bf_MIC_INPUT_SAMP_SIZE+bf_MIC_INPUT_SAMP_PAD];
tlong fileInSampleNum = bf_MIC_INPUT_SAMP_SIZE;
tlong fileInSampleIdx = 0;
#pragma DATA_SECTION (fileOutBufs, ".fileOutBufs")
#pragma DATA_ALIGN(fileOutBufs, 8)
linSample fileOutBufs[bf_MIC_INPUT_SAMP_SIZE+bf_MIC_INPUT_SAMP_PAD];
tlong fileOutSampleNum = bf_MIC_INPUT_SAMP_SIZE;
tlong fileOutSampleIdx = 0;

/* varibles for ASNR */
void *asnrInst[NUM_BF_VIRTUAL_MIC];
void *nr_handle[NUM_BF_VIRTUAL_MIC];
linSample vpeIOBufs[vpe_SIM_MAX_SAMPLES];
vpeSimConfig_t  vpeSimC = {
  TRUE,				  /* bf_on         */
  TRUE,				  /* nr_on          */
  TRUE,				  /* mss_on         */
  TRUE,                /* vau_on         */
  0,                    /* sample_cnt     */
  16000,                /* Sampling rate in Hz                        */
  /* BF size configure parameters                            */
  {
	bf_SAMP_RATE_16K,
	NUM_BF_ANALOG_MIC,
	SYS_BF_FILTER_LENGTH,
	bf_TYPE_FIXED,
  },
  /* BF control parameters                            */
  {
	bf_CTL_CONFIG|bf_CTL_MICS, /* Enable BF, operation mode change */
	bf_CTL_CFG_ENABLE|bf_CTL_CFG_OPERATION,
	bf_ENABLE|bf_OPT_NORMAL,
	0x00ff, /* 8 analog mics */
  },
  /* ASNR parameters                            */
  {
    1,                        /* Sampling frequency 0: 8Khz; 1: 16Khz */
    5,                        /* ASNR Signal delay                    */
    10,                       /* ASNR Frequency bands boundary bin 1  */
    32,                       /* ASNR Frequency bands boundary bin 2  */
    9,                        /* ASNR Maximum attenuation in band 1   */
    9,                        /* ASNR Maximum attenuation in band 2   */
    9,                        /* ASNR Maximum attenuation in band 3   */
    31129,                    /* ASNR Maximum signal update rate      */
    29818,                    /* ASNR Minimum signal update rate      */
    -75,                      /* ASNR Noise threshold                 */
    150,                      /* ASNR Noise hangover                  */
  },
  /* MSS control parameters */
  {
    mss_CTL_SWITCH_THRESH|mss_CTL_SWITCH_DURATION|mss_CTL_SWITCH_HNAGOVER, /* bitfield */
    {0x0003, mss_CTL_MODES_DEFAULT},    /* mode {mask, value} */
    {mss_SRC_MIC_FIXED, 0},   /* source {group, index} */
    250,                      /* switch thresh */
    500,                      /* switch duration */
    500,                      /* switch hangover */
  },
  TRUE,                 /* Flag indicating more samples for simulation*/
  160,                    /* Simulation frame size, input               */
  160,                    /* Simulation frame size, output              */
  0,                      /* to measure fixed overhead in cycle measurement */
  {0, 0},                 /* BF's cycle statistics */
  {0, 0},                 /* ASNR's cycle statistics */
  {0, 0},                 /* MSS's cycle statistics */
  {0, 0},                 /* VAU's cycle statistics */
  0,                   /* cycle output file pointer */
};
vpeSimConfig_t *vpeSim = &vpeSimC;

/* Allocate memory for NR buffers - size and alignment, for C55x/C6x */
#pragma DATA_ALIGN  (nr_buff0, NR_SIM_BUF0_ALGN)
tword nr_buff0[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF0_SIZE];
#pragma DATA_ALIGN  (nr_buff1, NR_SIM_BUF1_ALGN)
tword nr_buff1[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF1_SIZE];
#pragma DATA_ALIGN  (nr_buff2, NR_SIM_BUF2_ALGN)
tword nr_buff2[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF2_SIZE];
#pragma DATA_ALIGN  (nr_buff3, NR_SIM_BUF3_ALGN)
tword nr_buff3[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF3_SIZE];
#pragma DATA_ALIGN  (nr_buff4, NR_SIM_BUF4_ALGN)
tword nr_buff4[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF4_SIZE];
#pragma DATA_ALIGN  (nr_buff5, NR_SIM_BUF5_ALGN)
tword nr_buff5[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF5_SIZE];
#pragma DATA_ALIGN  (nr_buff6, NR_SIM_BUF6_ALGN)
tword nr_buff6[NUM_BF_VIRTUAL_MIC][NR_SIM_BUF6_SIZE];

/* varibles for MSS */
void  *mssInst;
#pragma DATA_ALIGN(mss_buf, 8);
mssbuf_t mss_buf;
void *beam_mics[NUM_BF_VIRTUAL_MIC];
linSample data_rx_sync[vpe_SIM_MAX_SAMPLES];  /* reference signal */

/* varibles for VAU */
config_t  config = {
  0,  /* xdaisFlag */
  0,  /* don't use default configuration for xdais */
  80, /* 10 ms default frame size at 8 KHz */
  2,  /* 8 KHz */

  /* vauConfig */
  {
   ifvau_VALID_TYPE | ifvau_VALID_THRESH_OFFSET | ifvau_VALID_HOLD_OVER,
   ifvau_TYPE_ADAPTIVE,
   0,
   180},

  /* vauControl */
  {
   ifvau_VALID_CONTROL,
   ifvau_ENABLE
  }
#ifdef USE_CACHE
 ,1, /* doCacheFlush */
  1  /* doCacheEnable */
#endif
};

tint siu_alloc_bf_buffs(const ecomemBuffer_t *bufs_req, ecomemBuffer_t *bufs_aloc,
                         tint num_bufs);

/* Global simulation data */
char vpe_sim_global_str[vpe_SIM_MAX_STRING];
char vpe_sim_print_str[vpe_SIM_MAX_STRING];

/*-----------------------------------------------------------------
 * Function:  vpe_print
 *
 * Description: Utility to provide static progress information.
 *-----------------------------------------------------------------*/
void vpe_print (char *str)
{
    printf (str);
}

/*-----------------------------------------------------------------
 * Function:  vpe_iprint
 *
 * Description: Utility to provide integer progress information.
 *-----------------------------------------------------------------*/
void vpe_iprint (char *str, tlong ivar)
{
  sprintf   (vpe_sim_print_str, str, ivar);
  vpe_print (vpe_sim_print_str);
}

/*-----------------------------------------------------------------
 * Function:  vpe_halt
 *
 * Description: Utility to provide simulation breakpoint.
 *-----------------------------------------------------------------*/
void vpe_halt (tbool *exec)
{
  /* Wait for further actions - exit when user sets "halt" to FALSE */
  while (!(*exec)) {
    asm ("  NOP");
    asm ("Set_BP_Here:  ");
    asm ("  NOP");
    asm ("  NOP");
  }
}

void vpe_sim_flush_cache() {
#if _VPESIM_C64||_VPESIM_C64_BIG_ENDIAN||_VPESIM_C64P||_VPESIM_C64P_BIG_ENDIAN |_VPESIM_C66||_VPESIM_C66_BIG_ENDIAN
  /* flush cache */
  memarchcfg_cacheFlush();
#endif
}

/*-----------------------------------------------------------------
 * Function:  vpe_banner_print
 *                            
 * Description: Utility to provide major progress information.
 *-----------------------------------------------------------------*/
static inline void vpe_banner_print (char *str)
{
  vpe_print ("\n");
  vpe_print ("------------------------------\n");
  vpe_print (str);
  vpe_print ("------------------------------\n");
  vpe_print ("\n");
}

void siu_print_buff_usage(const ecomemBuffer_t *bufs, tint num_bufs)
{
  int i;

  printf("    Buffer    Size(twords)    Alignment    Volatile   address\n");
  for(i=0; i<num_bufs; i++) {
    printf("     %3d       %7d         %4d       ",
                 i, (int)bufs[i].size, (int)bufs[i].log2align);
    if(bufs[i].volat)
      printf("    yes");
    else
      printf("    no ");

	printf("    0x%x\n", bufs[i].base);
  }
}

void siu_print_cycles(void)
{
  printf("-----------------------------------------------------------------\n");
  printf("Performance Data for %d channel analog input and %d virtual mics:\n",
		  NUM_BF_ANALOG_MIC, NUM_BF_VIRTUAL_MIC);
  printf("-----------------------------------------------------------------\n");
  siu_print_cycles_util(&vpeSim->cyclesBf, "BF", vpeSim->cyclesOverhead);
  siu_print_cycles_util(&vpeSim->cyclesAsnr, "ASNR", vpeSim->cyclesOverhead);
  siu_print_cycles_util(&vpeSim->cyclesMss, "MSS", vpeSim->cyclesOverhead);
  siu_print_cycles_util(&vpeSim->cyclesVau, "VAU", vpeSim->cyclesOverhead);
  printf("Sum MaxCycles = %ld AvgCycles = %ld\n",
         ((vpeSim->cyclesBf.max - vpeSim->cyclesOverhead)+
		 (vpeSim->cyclesAsnr.max - vpeSim->cyclesOverhead)+
		 (vpeSim->cyclesMss.max - vpeSim->cyclesOverhead)+
		 (vpeSim->cyclesVau.max - vpeSim->cyclesOverhead)),
         ((vpeSim->cyclesBf.avg - vpeSim->cyclesOverhead)+
		 (vpeSim->cyclesAsnr.avg - vpeSim->cyclesOverhead)+
		 (vpeSim->cyclesMss.avg - vpeSim->cyclesOverhead)+
		 (vpeSim->cyclesVau.avg - vpeSim->cyclesOverhead)));
}

/******************************************************************************
 * FUNCTION PURPOSE: Init heap memory.
 ******************************************************************************
 * DESCRIPTION:
 *
 *  void bf_mem_heap_init (
 *    bfMemHeap_t *ptr,
 *    tword *heapBase,
 *    tulong size)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/

void bf_mem_heap_init (bfMemHeap_t *ptr, tword *heapBase, tulong size)
{
  ptr->base = heapBase;
  ptr->size = size;
  ptr->indx = 0;
  /* Note : memory is not initialized deliberately to expose
            uninitialized memory read problems */
}

void bf_mem_init_heap (void)
{
  /* Internal heap */
  bf_mem_heap_init (&bfIntHeap, bfIntMem, bf_INT_HEAP_SIZE);
  /* External heap */
  bf_mem_heap_init (&bfExtHeap, bfExtMem, bf_EXT_HEAP_SIZE);
}

/******************************************************************************
 * FUNCTION PURPOSE: align memory
 ******************************************************************************
 * DESCRIPTION:
 *
 *  void *bf_mem_align (
 *    void *addr,
 *    tuint lin_align)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/

void *bf_mem_align (void *addr, tuint lin_align)
{
  tulong align_addr;

  align_addr = ((tulong) addr + lin_align - 1UL) & (0xFFFFFFFFUL - lin_align + 1UL);
  return ((void *) align_addr);
}

/******************************************************************************
 * FUNCTION PURPOSE: allocate heap memory
 ******************************************************************************
 * DESCRIPTION:
 *
 *  tword *bf_mem_heap_alloc (
 *    bfMemHeap_t *ptr,
 *    tint size)          - channel number (1 to SIU_MAX_CHANNELS)
 *
 *****************************************************************************/

tword *bf_mem_heap_alloc (bfMemHeap_t *ptr, tint size)
{
  tword *alloc = NULL;

  if ((ptr->indx + size) <= ptr->size) {
    alloc = &ptr->base[ptr->indx];
    ptr->indx += size;
  }
  return (alloc);
}

void *bf_mem_alloc_align (tint size, tint mclass, tint linAlign)
{
  void *allocPtr;
  bfMemHeap_t *ptr;

  if (mclass == ecomem_CLASS_INTERNAL) {
    ptr = &bfIntHeap;
  }
  else if (mclass == ecomem_CLASS_EXTERNAL) {
    ptr = &bfExtHeap;
  }
  else {
    printf("Unrecognized memory class, exiting\n");
    exit (0);
  }

  if ((allocPtr = (void *) bf_mem_heap_alloc (ptr, size + linAlign - 1)) == NULL) {
    printf("Not enough heap, exiting\n");
    exit (0);
  }

  return (bf_mem_align (allocPtr, linAlign));
}

/******************************************************************************
 * FUNCTION PURPOSE: Allocate buffers for bf instance
 *
 *****************************************************************************/
tint siu_alloc_bf_buffs(const ecomemBuffer_t *bufs_req, ecomemBuffer_t *bufs_aloc,
                         tint num_bufs)
{
  tint err_code;
  tint i;
  tuint  linAlign, log2align;

  err_code = bf_NOERR;
  for (i = 0; i < num_bufs; i++) {
    log2align = bufs_req[i].log2align;
    linAlign = (tuint) 1 << log2align;

    bufs_aloc[i].mclass    = bufs_req[i].mclass;   /* internal memory for all  */
    bufs_aloc[i].log2align = bufs_req[i].log2align;/* meet alignment requirement */
    bufs_aloc[i].volat     = FALSE;                /* initialized to permanent */
    bufs_aloc[i].size      = bufs_req[i].size;     /* size of the buffer */
    bufs_aloc[i].base      = bf_mem_alloc_align (bufs_aloc[i].size, bufs_aloc[i].mclass, linAlign);
  }

  bufs_aloc[3].volat     = TRUE;                /* initialized to permanent */
  bufs_aloc[4].volat     = TRUE;                /* initialized to permanent */

  return(err_code);
} /* siu_alloc_bf_buffs */

Fract spchLevdB = frct_MAX, noiseLevdB = frct_MAX;
UFract Nthresh = frct_MAX;

tbool bf_getNextFrame(void *framePtr[], tint num_tuint_in)
{
	tint i, j;
	linSample *tempFramePtr;
	tuint temp;

	/* do we have enough samples */
	if (fileInSampleNum<(fileInSampleIdx+num_tuint_in))
		return FALSE;

	/* read one frame from each mic */
	for (i=0; i<NUM_BF_ANALOG_MIC; i++)
	{
		tempFramePtr = (linSample *)framePtr[i];
		for (j=0; j<num_tuint_in; j++)
		{
			temp = fileInBufs[i][fileInSampleIdx+j];
			*(tempFramePtr+j) = (((temp&0xFF00)>>8)|((temp&0x00FF)<<8));
		}
	}
	fileInSampleIdx += num_tuint_in;
	return TRUE;
}

tbool bf_putNextFrame(void *framePtr, tint num_tuint_in)
{
  tint j;
  linSample *tempFramePtr;
  tuint temp;

  /* do we have enough samples */
  if (fileOutSampleNum<(fileOutSampleIdx+num_tuint_in))
    return FALSE;

  /* write one frame */
  tempFramePtr = (linSample *)framePtr;
  for (j=0; j<num_tuint_in; j++)
  {
    temp = *(tempFramePtr+j);
    fileOutBufs[fileOutSampleIdx+j] = (((temp&0xFF00)>>8)|((temp&0x00FF)<<8));
  }
  fileOutSampleIdx += num_tuint_in;
  return TRUE;
}

void bf_init(void)
{
  tint  i;

  /* frame buffer and pointers init */
  for (i=0; i<NUM_BF_ANALOG_MIC; i++)
  {
    inFramePtr[i] = bfInFrame[i];
  }

  for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
  {
    buf_Inptr[i] = (linSample *)sigInBuffer[i];
  }
  buf_Outptr = (linSample *)sigOutBuffer;
  buf_OutptrMss = (linSample *)sigOutBufferMss;

  bf_mem_init_heap();
}

void bf_setup(tint virMicNum)
{
  tint  i, err_code;
  bfConfig_t bfCfg;
  Fract *filterPtr;

  /* Get buffer size for BF */
  err_code = bfGetSizes(&num_bufs_req_by_bf, &bufs_req_by_bf, &vpeSim->bf_size_cfg);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfGetSizes() with error code %d!\n", err_code);
    exit(1);
  }

  printf("bf required buffers:\n");
  siu_print_buff_usage(bufs_req_by_bf, num_bufs_req_by_bf);

  /* Check if there is enough memory allocated for bf */
  if (num_bufs_req_by_bf > MAX_NUM_BF_BUFFS) {
    printf("Memory allocated by SIU is not enough for bf!\n");
    exit(0);
  }

  /* Provide memory to bf through bfNew() */
  err_code = siu_alloc_bf_buffs(bufs_req_by_bf, bufs_aloc_for_bf,
							   num_bufs_req_by_bf);
  if (err_code != bf_NOERR) {
    printf("Error when allocating buffers for bf with error code %d!\n", err_code);
    exit(1);
  }
  printf("Buffers allocated by SIU for bf:\n");
  siu_print_buff_usage(bufs_aloc_for_bf, num_bufs_req_by_bf);

  /* Set up configurations for bfNew() */
  bf_new_cfg.handle  = (void *) siuMakeID(SIU_MID_BF, virMicNum);
  bf_new_cfg.sizeCfg = vpeSim->bf_size_cfg;

  /* Create a new BF instance */
  err_code = bfNew (&bfInst[virMicNum], num_bufs_req_by_bf, bufs_aloc_for_bf, &bf_new_cfg);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfNew() with error code %d!\n", err_code);
    exit(1);
  }

  /* Open the BF instance */
  bfCfg.sampling_rate = vpeSim->bf_size_cfg.max_sampling_rate;
  bfCfg.num_mics = vpeSim->bf_size_cfg.max_num_mics;

  /* Call bf API to open the bf instance */
  err_code = bfOpen (bfInst[virMicNum], &bfCfg);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfOpen()with error code %d!\n", err_code);
    exit(1);
  }

  /* Put in filter coefficients for BF */
  if (bfCfg.num_mics==12)
  {
    filterPtr = sysBfFilters[fltIdx[0][virMicNum]];
  } else if (bfCfg.num_mics==8)
  {
    filterPtr = sysBfFilters[fltIdx[1][virMicNum]];
  } else
  {
    printf("Error incorrect virtual mics(%d)!\n", bfCfg.num_mics);
    exit(1);
  }

  for (i=0; i<bfCfg.num_mics; i++)
  {
    bfPutFilter(bfInst[virMicNum], &filterPtr[SYS_BF_FILTER_LENGTH*i], bf_FG_BF, i, SYS_BF_FILTER_LENGTH);
  }

  /* Enable the BF instance */
  err_code = bfControl(bfInst[virMicNum], &vpeSim->bf_ctl);
  if (err_code != bf_NOERR) {
    printf("Error when configure BF with error code %d!\n", err_code);
    exit(1);
  }
}

void bf_closedown(tint virMicNum)
{
  tint err_code,i;
  ecomemBuffer_t bufs[MAX_NUM_BF_BUFFS];

  /* Close the BF instance */
  err_code = bfClose(bfInst[virMicNum]); /* the instance state is set to bf_CLOSED */
  if (err_code != bf_NOERR) {
    printf("Error when calling bfClose() with error code %d!\n", err_code);
    exit(1);
  }

  err_code = bfDelete (&bfInst[virMicNum], MAX_NUM_BF_BUFFS, bufs);
  if (err_code != bf_NOERR) {
    printf("Error when calling bfDelete() with error code %d!\n", err_code);
    exit(1);
  }

  for(i=0;i<MAX_NUM_BF_BUFFS;i++)
  {
    free(bufs[i].base);
  }
}

void asnr_setup(tint virMicNum)
{
  const ecomemBuffer_t *bufs_req_by_nr;
  ecomemBuffer_t        bufs_alloc_for_nr[VPE_NR_NUM_BUFS];
  asnrSizeConfig_t        nr_size_cfg;
  asnrNewConfig_t         nr_new_cfg;
  asnrOpenConfig_t        nr_open_cfg;
  tint                  num_bufs_req_by_nr, status;
  int i;
  tint srate;
  asnrControl_t nr_control_cfg;

  if (vpeSim->Fs == VPE_SIM_SAMPLE_FREQ_8000) {
	srate = asnr_SRATE_8K;
  }
  else {
	srate = asnr_SRATE_16K;
  }

  /* Get NR memory buffer requirements */
  nr_size_cfg.max_sampling_rate=srate;
  status = asnrGetSizes(&num_bufs_req_by_nr, &bufs_req_by_nr, &nr_size_cfg);
  if(status != asnr_NOERR) {
	printf("Error returned by asnrGetSizes() = %d\n",status);
	exit(0);
  }

  if(num_bufs_req_by_nr != VPE_NR_NUM_BUFS) {
	printf("NR needs %d buffers, but %d buffers are allocated!\n",
			num_bufs_req_by_nr, VPE_NR_NUM_BUFS);
	exit(0);
  }

  printf("Buffers requested by NR:\n");
  printf("    Buffer    Size(twords)    Alignment    Volatile\n");
  for(i=0; i<num_bufs_req_by_nr; i++) {
	printf("     %3d       %7d         %4d       ",
				 i, (int)bufs_req_by_nr[i].size, (int)bufs_req_by_nr[i].log2align);
	if(bufs_req_by_nr[i].volat)
	  printf("    yes\n");
	else
	  printf("    no \n");
  }

  /* Statically allocate buffers for NR */
  for (i=0; i<num_bufs_req_by_nr; i++) {
	bufs_alloc_for_nr[i].mclass = ecomem_CLASS_INTERNAL;
	bufs_alloc_for_nr[i].volat  = FALSE;         /* initialized to permanent */
  }
  bufs_alloc_for_nr[0].base      = &nr_buff0[virMicNum][0];
  bufs_alloc_for_nr[0].size      = NR_SIM_BUF0_SIZE;
  bufs_alloc_for_nr[0].log2align = NR_SIM_BUF0_ALGN_LOG2;

  bufs_alloc_for_nr[1].base      = &nr_buff1[virMicNum][0];
  bufs_alloc_for_nr[1].size      = NR_SIM_BUF1_SIZE;
  bufs_alloc_for_nr[1].log2align = NR_SIM_BUF1_ALGN_LOG2;
  bufs_alloc_for_nr[1].volat     = TRUE;

  bufs_alloc_for_nr[2].base      = &nr_buff2[virMicNum][0];
  bufs_alloc_for_nr[2].size      = NR_SIM_BUF2_SIZE;
  bufs_alloc_for_nr[2].log2align = NR_SIM_BUF2_ALGN_LOG2;
  bufs_alloc_for_nr[2].volat     = TRUE;

  bufs_alloc_for_nr[3].base      = &nr_buff3[virMicNum][0];
  bufs_alloc_for_nr[3].size      = NR_SIM_BUF3_SIZE;
  bufs_alloc_for_nr[3].log2align = NR_SIM_BUF3_ALGN_LOG2;
  bufs_alloc_for_nr[3].volat     = TRUE;

  bufs_alloc_for_nr[4].base      = &nr_buff4[virMicNum][0];
  bufs_alloc_for_nr[4].size      = NR_SIM_BUF4_SIZE;
  bufs_alloc_for_nr[4].log2align = NR_SIM_BUF4_ALGN_LOG2;

  bufs_alloc_for_nr[5].base      = &nr_buff5[virMicNum][0];
  bufs_alloc_for_nr[5].size      = NR_SIM_BUF5_SIZE;
  bufs_alloc_for_nr[5].log2align = NR_SIM_BUF5_ALGN_LOG2;

  bufs_alloc_for_nr[6].base      = &nr_buff6[virMicNum][0];
  bufs_alloc_for_nr[6].size      = NR_SIM_BUF6_SIZE;
  bufs_alloc_for_nr[6].log2align = NR_SIM_BUF6_ALGN_LOG2;

  /* Create NR instance */
  nr_new_cfg.handle  = &nr_handle[virMicNum];
  nr_new_cfg.sizeCfg = nr_size_cfg;
  status = asnrNew(&asnrInst[virMicNum], num_bufs_req_by_nr, bufs_alloc_for_nr, &nr_new_cfg);
  if (status != asnr_NOERR) {
	printf("Error returned by asnrNew() = %d\n",status);
	exit(0);
  }

  printf("Buffers allocated for NR:\n");
  printf("    Buffer    Size(twords)    Alignment    Volatile   address\n");
  for(i=0; i<num_bufs_req_by_nr; i++) {
	printf("     %3d       %7d         %4d       ",
				 i, (int)bufs_alloc_for_nr[i].size, (int)bufs_alloc_for_nr[i].log2align);
	if(bufs_alloc_for_nr[i].volat)
	  printf("    yes   ");
	else
	  printf("    no    ");

	printf("    0x%x\n", bufs_alloc_for_nr[i].base);
  }

  /* Open newly created NR instance */
  nr_open_cfg.sampling_rate = srate;
  status = asnrOpen(asnrInst[virMicNum], &nr_open_cfg);
  if (status != asnr_NOERR) {
	printf("Error returned by asnrOpen() = %d!\n", status);
	exit(0);
  }

  /* Configure NR parameters */
  nr_control_cfg.valid_bitfield =   asnr_CTL_VALID_ENABLE
								  | asnr_CTL_VALID_SAMP_RATE
								  | asnr_CTL_VALID_DELAY
								  | asnr_CTL_VALID_BAND_BIN1
								  | asnr_CTL_VALID_BAND_BIN2
								  | asnr_CTL_VALID_BAND1_MAX_ATTEN
								  | asnr_CTL_VALID_BAND2_MAX_ATTEN
								  | asnr_CTL_VALID_BAND3_MAX_ATTEN
								  | asnr_CTL_VALID_SIG_UPD_RATE_MIN
								  | asnr_CTL_VALID_SIG_UPD_RATE_MAX
								  | asnr_CTL_VALID_NOISE_THRESH
								  | asnr_CTL_VALID_NOISE_HANGOVER;
  nr_control_cfg.enable          = 1;
  nr_control_cfg.samp_rate       = vpeSim->asnrParam.samp_rate;
  nr_control_cfg.delay           = vpeSim->asnrParam.delay;
  nr_control_cfg.band_bin1       = vpeSim->asnrParam.band_bin1;
  nr_control_cfg.band_bin2       = vpeSim->asnrParam.band_bin2;
  nr_control_cfg.band1_max_atten = vpeSim->asnrParam.band1_max_atten;
  nr_control_cfg.band2_max_atten = vpeSim->asnrParam.band2_max_atten;
  nr_control_cfg.band3_max_atten = vpeSim->asnrParam.band3_max_atten;
  nr_control_cfg.sig_upd_rate_max= vpeSim->asnrParam.sig_upd_rate_max;
  nr_control_cfg.sig_upd_rate_min= vpeSim->asnrParam.sig_upd_rate_min;
  nr_control_cfg.noise_thresh    = vpeSim->asnrParam.noise_thresh;
  nr_control_cfg.noise_hangover  = vpeSim->asnrParam.noise_hangover;

  status = asnrControl(asnrInst[virMicNum], &nr_control_cfg);
  if (status != asnr_NOERR) {
	printf("Error returned by asnrControl = %d!\n", status);
	exit(0);
  }
}

void asnr_closedown(tint virMicNum)
{
  ecomemBuffer_t bufs_returned_by_nr[VPE_NR_NUM_BUFS];
  tint           status, error;

  status = asnrClose(asnrInst[virMicNum]);
  if(status != asnr_NOERR) {
	printf("Error returned by asnrClose() = %d!\n", status);
	exit(0);
  }

  status = asnrDelete(&asnrInst[virMicNum], VPE_NR_NUM_BUFS, bufs_returned_by_nr);
  if(status != asnr_NOERR) {
	printf("Error returned by asnrDelete() = %d!\n", status);
	exit(0);
  }

  error =    (bufs_returned_by_nr[0].base != nr_buff0[virMicNum])
		   | (bufs_returned_by_nr[1].base != nr_buff1[virMicNum])
		   | (bufs_returned_by_nr[2].base != nr_buff2[virMicNum])
		   | (bufs_returned_by_nr[3].base != nr_buff3[virMicNum])
		   | (bufs_returned_by_nr[4].base != nr_buff4[virMicNum])
		   | (bufs_returned_by_nr[5].base != nr_buff5[virMicNum]);
///		   | (bufs_returned_by_nr[6].base != nr_buff6[virMicNum]);
  if(error) {
	printf("Buffers returned by asnrDelete() are wrong!\n");
	exit(0);
  }
}

void mss_setup(void)
{
  const ecomemBuffer_t *bufs_req_by_mss;
  ecomemBuffer_t        bufs_aloc_by_siu[SIU_NUM_MSS_BUFFS];
  mssSizeConfig_t   mss_size_cfg;
  mssNewConfig_t    mss_new_cfg;
  tint err_code, num_bufs_req_by_mss;
  mssControl_t  mss_ctl_cfg;
  mssConfig_t   mss_open_cfg;

  /* Set up configurations for mssGetSizes() */
  mss_size_cfg.max_sampling_rate  = mss_SAMP_RATE_16K;
  mss_size_cfg.max_num_mic_fixed  = NUM_BF_VIRTUAL_MIC;
  mss_size_cfg.max_num_mic_remote = 0;
  mss_size_cfg.max_num_mic_clean  = 0;
  mss_size_cfg.max_num_beam       = 0;

  /* Call API function mssGetSizes() to get memory requirements:
	 - num_bufs_req_by_mss: number of buffers required by MSS
	 - bufs_req_by_mss:   pointer to buffer descriptors defined inside MSS */
  err_code = mssGetSizes(&num_bufs_req_by_mss, &bufs_req_by_mss, &mss_size_cfg);

  printf("MSS required buffers:\n");
  siu_print_buff_usage(bufs_req_by_mss, num_bufs_req_by_mss);

  /* Check if there is enough memory allocated for MSS */
  if(   num_bufs_req_by_mss > SIU_NUM_MSS_BUFFS
	 || bufs_req_by_mss[0].size > sizeof(mssbuf_t)) {
	printf("Memory allocated by SIU is not enough for MSS!\n");
	exit(0);
  }

  /* Provide memory to MSS through mssNew() */
  bufs_aloc_by_siu[0].mclass    = ecomem_CLASS_INTERNAL;
  bufs_aloc_by_siu[0].log2align = bufs_req_by_mss[0].log2align;
  bufs_aloc_by_siu[0].volat     = FALSE;
  bufs_aloc_by_siu[0].size      = sizeof(mssbuf_t);
  bufs_aloc_by_siu[0].base      = &mss_buf;

  printf("Buffers allocated by SIU for MSS:\n");
  siu_print_buff_usage(bufs_aloc_by_siu, num_bufs_req_by_mss);

  /* Set up configurations for mssNew() */
  mss_new_cfg.handle  = (void *) siuMakeID(SIU_MID_MSS, 0);
  mss_new_cfg.sizeCfg = mss_size_cfg;

  /* Call API function mssNew() to create an MSS instance:
	 - pass buffer descriptors that are set by SIU
	 - pass number of buffer descriptors   */
  err_code = mssNew (&mssInst, num_bufs_req_by_mss, bufs_aloc_by_siu, &mss_new_cfg);
  if (err_code != mss_NOERR) {
	printf("Error when calling mssNew() with error code %d!\n", err_code);
	exit(1);
  }

  /* Set up configuration for mssOpen() */
  mss_open_cfg.sampling_rate  = (vpeSim->asnrParam.samp_rate==1) ? mss_SAMP_RATE_16K : mss_SAMP_RATE_8K;
  mss_open_cfg.num_mic_fixed  = NUM_BF_VIRTUAL_MIC;
  mss_open_cfg.num_mic_remote = 0;
  mss_open_cfg.num_mic_clean  = 0;
  mss_open_cfg.num_mic_array  = 0;
  mss_open_cfg.num_beam       = 0;

  err_code = mssOpen (mssInst, &mss_open_cfg);
  if (err_code != mss_NOERR) {
	printf("Error when calling mssOpen() with error code %d!\n", err_code);
	exit(1);
  }

  /* Set 1st hands-free mic as the selected source for MSS */
  mss_ctl_cfg.valid_bitfield = mss_CTL_SELECT_SRC;
  mss_ctl_cfg.source.group   = vpeSim->mss_ctl_cfg.source.group;
  mss_ctl_cfg.source.index   = vpeSim->mss_ctl_cfg.source.index;
  err_code = mssControl (mssInst, &mss_ctl_cfg);

  mss_ctl_cfg.valid_bitfield = vpeSim->mss_ctl_cfg.valid_bitfield;

  mss_ctl_cfg.switch_thresh   = vpeSim->mss_ctl_cfg.switch_thresh;
  mss_ctl_cfg.switch_duration = vpeSim->mss_ctl_cfg.switch_duration;
  mss_ctl_cfg.switch_hangover = vpeSim->mss_ctl_cfg.switch_hangover;
  err_code = mssControl (mssInst, &mss_ctl_cfg);

  if (err_code != mss_NOERR) {
    printf("Error when calling mssControl() to configure MSS with error code %d!\n",
    err_code);
	exit(1);
  }
}

void mss_closedown(void)
{
  ecomemBuffer_t bufs_returned_by_siu[SIU_NUM_MSS_BUFFS];
  tint err_code, error;

  err_code = mssClose(mssInst);
  if (err_code != mss_NOERR) {
    printf("Error when calling mssClose() with error code %d!\n", err_code);
    exit(1);
  }

  err_code = mssDelete(&mssInst, SIU_NUM_MSS_BUFFS, bufs_returned_by_siu);
  if (err_code != mss_NOERR) {
    printf("Error when calling mssDelete() with error code %d!\n", err_code);
    exit(1);
  }

  error = (bufs_returned_by_siu[0].base != &mss_buf);
  if(error) {
    printf("Buffers returned by mssDelete() are wrong!\n");
    exit(0);
  }
}

void vau_setup(void)
{
  /* print the VAU version */
  printf("Version Information of modules tested:\n");
  printf("VAU %d.%d.%d.%d\n",
    ti_mas_vau_Version_MAJOR, ti_mas_vau_Version_MINOR,
    ti_mas_vau_Version_XVERTICAL, ti_mas_vau_Version_PATCH);

  /* print XDAIS or ECO */
  if (config.xdaisFlag) {
    printf("Using XDAIS APIs\n");
  }
  else {
    printf("Using ECO APIs\n");
  }

  hw_init();

  siu_open();

  /* configure VAU */
  siu_new_open_config_modules(&config);
}

void vau_closedown(void)
{
  siu_close();
}

/*-----------------------------------------------------------------
 * Filename:  main
 *                            
 * Description: Test the vpe API functions.
 *-----------------------------------------------------------------*/
void main (void)
{
  tulong sample_total;
  tbool sigIn_data; 
  tuint second_count;
  tuint sample_count, num_tuint_in, num_tuint_out;
  tint  i, j, retVal;
#ifdef PROFILE_CYCLES
  volatile tlong cycles;
#endif

#ifdef PROFILE_CYCLES
  if ((vpeSim->outCyclesFilePtr =
    fopen(nmu_SIM_LOGCYC_FNAME, "wt")) == NULL) {
    printf(" Error opening cycles.txt\n");
    exit(0);
  }

  /* measure measurement overhead of cycles */
  cycles = profile_getCPUcycleCount();

  cycles = (profile_getCPUcycleCount() - cycles);
  vpeSim->cyclesOverhead = cycles;
#endif

  if (vpeSim->bf_on)
  {
    /* BF initialization */
    bf_init();

    /* setup BF */
    for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
	    bf_setup(i);
  }

  if (vpeSim->nr_on)
  {
    /* setup VPE for ASNR */
    for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
	    asnr_setup(i);
  }

  if (vpeSim->mss_on)
  {
    /* setup MSS */
    mss_setup();
  }

  if (vpeSim->vau_on)
  {
    /* setup VAU */
    vau_setup();
  }
  
  /* Simulation pass initialization */
  vpeSim->data = TRUE;

  /* Single test/example simulation loop */
  second_count = 0;
  sample_count = 0;
  fileInSampleIdx = 0;
  fileOutSampleIdx = 0;
  while (vpeSim->data) {
    vpe_sim_flush_cache();

	/* Read input frame data from file IO buffers */
	num_tuint_in = vpeSim->frame_size_in;
	sigIn_data = bf_getNextFrame((void **)inFramePtr, num_tuint_in);
	vpeSim->data = sigIn_data;

    if (vpeSim->bf_on)
    {
#ifdef PROFILE_CYCLES
      cycles = profile_getCPUcycleCount();
#endif	  /* BF processing the currenct frame  for each virtual mic */
	  for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
	  {
        retVal = bfProcess(bfInst[i], (void **)inFramePtr, buf_Inptr[i]);
		if (retVal!=bf_NOERR)
		{
		  printf("Error returned by bfProcess() = %d!\n", retVal);
		  exit(0);
		}
	  }
#ifdef PROFILE_CYCLES
      cycles = (profile_getCPUcycleCount() - cycles);
      siu_process_cycles(cycles, &vpeSim->cyclesBf);
      fprintf(vpeSim->outCyclesFilePtr, "BF:	%ld\n", (long) cycles);
#endif
    } else
    {
    	/* copy analog mics instead */
    	for (j=0; j<NUM_BF_ANALOG_MIC; j++)
    	{
	      for (i=0;i<num_tuint_out;i++)
	    	buf_Inptr[j][i] = inFramePtr[j][i];
	    }
    }

    if(vpeSim->nr_on) {
#ifdef PROFILE_CYCLES
        cycles = profile_getCPUcycleCount();
#endif	  /* BF processing the currenct frame  for each virtual mic */
        /* Run NR if enabled */
    	for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
    	{
	      retVal = asnrProcess(asnrInst[i], buf_Inptr[i], buf_Inptr[i]);
		  if (retVal != asnr_NOERR) {
			printf("Error returned by asnrProcessn() = %d!\n", retVal);
			exit(0);
		  }
      	}
#ifdef PROFILE_CYCLES
        cycles = (profile_getCPUcycleCount() - cycles);
        siu_process_cycles(cycles, &vpeSim->cyclesAsnr);
        fprintf(vpeSim->outCyclesFilePtr, "ASNR:	%ld\n", (long) cycles);
#endif
    }
      
	if (vpeSim->mss_on)
	{
#ifdef PROFILE_CYCLES
      cycles = profile_getCPUcycleCount();
#endif	  /* BF processing the currenct frame  for each virtual mic */
      /* MSS processing here */
	  /* prepare for MSS processing - only one soure for the case of mic-array */
	  for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
		beam_mics[i] = (void *)&buf_Inptr[i][0];

	  /* multi-source selection module to select the optimum source (mic) */
	  retVal = mssProcess(mssInst, buf_Outptr, data_rx_sync, beam_mics, NULL, NULL, NULL, NULL);
	  if (retVal != mss_NOERR) {
		printf("Error returned by mssProcess() = %d!\n", retVal);
		exit(0);
	  }
#ifdef PROFILE_CYCLES
      cycles = (profile_getCPUcycleCount() - cycles);
      siu_process_cycles(cycles, &vpeSim->cyclesMss);
      fprintf(vpeSim->outCyclesFilePtr, "MSS:	%ld\n", (long) cycles);
#endif
	} else
	{
	  /* copy to the virtual mic 0 to output buffer */
	  for (i=0;i<num_tuint_out;i++){
	    buf_Outptr[i] = buf_Inptr[0][i];
	  }
	}

	if (vpeSim->vau_on)
	{
#ifdef PROFILE_CYCLES
      cycles = profile_getCPUcycleCount();
#endif	  /* BF processing the currenct frame  for each virtual mic */
      /* VAU processing here */
      /* first half frame */
      siuInstVau.frameCount++;
      siu_process(buf_Outptr, &buf_OutptrMss);
      /* second half frame */
      siuInstVau.frameCount++;
      buf_Outptr2 = &buf_OutptrMss[vpeSim->frame_size_out/2];
      siu_process(&buf_Outptr[vpeSim->frame_size_out/2], &buf_Outptr2);
#ifdef PROFILE_CYCLES
      cycles = (profile_getCPUcycleCount() - cycles);
      siu_process_cycles(cycles, &vpeSim->cyclesVau);
      fprintf(vpeSim->outCyclesFilePtr, "VAU:	%ld\n", (long) cycles);
#endif
	}

    num_tuint_out = vpeSim->frame_size_out;
	/* copy to a output buffer */
    for (i=0;i<num_tuint_out;i++){
      buf_OutptrMss[i] = buf_Outptr[i];
    }

	/* save the frame to the output file buffer */
    bf_putNextFrame(buf_OutptrMss, num_tuint_out);
 
    /* Update sample count and notify time progress */
    sample_count += vpeSim->frame_size_in;
    if (sample_count >= vpeSim->Fs) {
      sample_count -= vpeSim->Fs;
      second_count++;
      printf (" - Progress: %d seconds\n", second_count);
    }

    /* Check for simulation halt */
    sample_total = (tulong) second_count * vpeSim->Fs + sample_count;
    vpeSim->sample_cnt = sample_total;
  } /* vpeSim->data */
        
#ifdef PROFILE_CYCLES
  siu_print_cycles();
  fclose(vpeSim->outCyclesFilePtr);
#endif

  /* Progress (verbose only) */
  vpe_banner_print ("Simulation:  END\n");
  /* End of current simulation test/example */

  if (vpeSim->vau_on)
  {
    /* VAU close down */
    vau_closedown();
  }

  if (vpeSim->mss_on)
  {
    /* MSS close down */
    mss_closedown();
  }

  if (vpeSim->nr_on)
  {
    /* ASNR close down */
    for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
	  asnr_closedown(i);
  }

  if (vpeSim->bf_on)
  {
    /* BF close down */
    for (i=0; i<NUM_BF_VIRTUAL_MIC; i++)
	  bf_closedown(i);
  }

  /* End of simulation */
  printf("Simulation done.\n");
} /* main */

/* Nothing past this point */
