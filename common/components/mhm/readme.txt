Date: 12/12/2006          Jianzhong Xu 
Version 1.0.0.0

The initial version of the package has three files: mhm.h, c55x/mhmport.h, mhm.c.
1. mhm.h: This file was taken from the latest of main branch. It was chagned in branch 
          blr_ggxvipp_r001_04_12_01_videoDSP_framework_start. The change was ignored since that brach is for video.

2. mhmport.h: This file was taken from the latest of main branch. It is the same among all branches.

3. mhm.c: This file was taken from the latest of main branch. It was changed in branch 
          blr_ggxcore_r10_03_06_14_core_10_0_0_5_core10_2 and ggdcm_desktop_vc_port. 
          Changes were ignored sicne the changes were not done properly. They should be outside of mhm.c.