/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*=================================================================
 *  sysmss.c: MSS creation routines.
 *=================================================================*/

#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>

#include <types.h>
#include <ti/mas/util/ecomem.h>
//#include <ti/mas/aer/mss.h>

#include "mss/mss.h"
#include "sys.h"

/* Configuration for getSizes */
mssSizeConfig_t mssSizeConfig = {
  mss_SAMP_RATE_16K,    /* 16kHz sampling rate */
  SYS_VMICS_MAX,        /* Maximum # of fixed mics (will be providing virtual here! */
  0,                    /* No remote mics */
  0,                    /* No "clean" mics */
  0                     /* Beams not supported so we have to used fixed! */
};

/* Configuration for Open */
mssConfig_t mssConfig = {
  mss_SAMP_RATE_16K,    /* use 16kHz sampling rate */
  SYS_VMICS_MAX,        /* use one virtual mic only by default */
  0,                    /* No remote mics */
  0,                    /* No "clean" mics */
  SYS_MICS_MAX,         /* Microphone array has 8 mics */
  0                     /* Beams not supported so we used the fixed! */
};

mssControl_t mssCtl = {
  (mss_CTL_SWITCH_THRESH | mss_CTL_SWITCH_DURATION | mss_CTL_SWITCH_HNAGOVER),
  {0, 0},   /* do not use modes */
  {0, 0},   /* do not use source */
  250,      /* sw-thresh in ms */
  500,      /* sw-duration in ms */
  500       /* sw-hangover in ms */
};

/*=================================================================
 *  void sysMssCreate(void)     Create beamformers
 *=================================================================*/
void sysMssCreate(void)
{
  int err;
  tint nbufs;
  const ecomemBuffer_t  *bufs;
  mssNewConfig_t mssNewConfig;
//  bfControl_t   bfCtl;

  System_printf("...Initializing MSS\n");
  System_flush();

  /* Configure MSS */
  err = mssGetSizes(&nbufs, &bufs, &mssSizeConfig);
  if (err != mss_NOERR) {
    System_printf("*** MSS getSizes error: %d\n", err);
    BIOS_exit(0);
  }
  if (nbufs > SYS_COMP_MAXBUFS) {
    System_printf("*** not enough buffer descriptors");
    BIOS_exit(0);
  }

  /* Allocate memory for MSS */
  mssNewConfig.sizeCfg = mssSizeConfig;   /* Use same configuration */
  err = sysHeapAllocAll(nbufs, sysCompBufs, (const void*)bufs);
  SYS_CHECK_ERROR(err);
  
  /* Give memory to MSS */
  mssNewConfig.handle = (void*)0;     /* Indicate instance 0 */
  sysContext.mssInst_p = NULL;
  err = mssNew(&sysContext.mssInst_p, nbufs, sysCompBufs, &mssNewConfig);
  if (err != mss_NOERR) {
    System_printf("*** MSS New error: %d\n", err);
    BIOS_exit(0);
  }

  /* Open MSS for business */
  err = mssOpen(sysContext.mssInst_p,&mssConfig);
  if (err != mss_NOERR) {
    System_printf("*** MSS open error: %d\n", err);
    BIOS_exit(0);
  }
  /* At this point MSS is open */

  /* Reconfigure hangovers, etc. */
  err = mssControl(sysContext.mssInst_p, &mssCtl);
  if (err != mss_NOERR) {
    System_printf("*** MSS control error: %d\n", err);
    BIOS_exit(0);
  }

  System_printf("Done with MSS\n");
  System_flush();

} /* sysMssCreate */

/* nothing past this point */

