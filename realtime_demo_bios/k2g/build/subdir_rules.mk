################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
configPkg/linker.cmd: ../K2G_bf_rt.cfg
	@echo 'Building file: $<'
	@echo 'Invoking: XDCtools'
	"${XDC_INSTALL_PATH}/xs" --xdcpath="${EDMA3LLD_BIOS6_INSTALLDIR}/packages;C:/ti/ccsv6/ccs_base;${BIOS_INSTALL_PATH}/packages;${UIA_INSTALL_PATH}/packages;${PDK_INSTALL_PATH};${AER_INSTALL_PATH}/packages;${VOLIB_INSTALL_PATH}/packages;${XDAIS_INSTALL_PATH}/packages;${XDAIS_INSTALL_PATH}/examples;../../../realtime_demo_bios/platforms/packages;" xdc.tools.configuro -o configPkg -t ti.targets.elf.C66 -p evmTCI66AK2G02Custom -r debug -c "${C6X_GEN_INSTALL_PATH}" "$<"
	@echo 'Finished building: $<'
	@echo ' '

configPkg/compiler.opt: | configPkg/linker.cmd
configPkg: | configPkg/linker.cmd

fil.obj: ../../../common/components/fil.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="fil.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

mhm.obj: ../../../common/components/mhm/src/mhm.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="mhm.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

mss.obj: ../../../common/components/mss/src/mss.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="mss.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

mssproc.obj: ../../../common/components/mss/src/mssproc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="mssproc.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sys.obj: ../../../common/components/sys.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sys.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysasnr.obj: ../../../common/components/sysasnr.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sysasnr.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysbf.obj: ../../../common/components/sysbf.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sysbf.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysbfflt.obj: ../../../common/components/sysbfflt.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sysbfflt.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysdrc.obj: ../../../common/components/sysdrc.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sysdrc.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

syseram.obj: ../../../common/components/syseram.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="syseram.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysiram.obj: ../../../common/components/sysiram.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sysiram.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

sysmss.obj: ../../../common/components/sysmss.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="../../../common/components" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="sysmss.d" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


