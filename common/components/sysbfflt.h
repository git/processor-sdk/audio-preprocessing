/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* 
  sysbfflt.h: BF filter definitions for all angles of intereset
*/
#ifndef _SYSBFFLT_H
#define _SYSBFFLT_H

#include <xdc/std.h>                /* required for types.h */
#include <types.h>

/*======================================================================
 * Filter angles that are supported
 *======================================================================*/

#define SYS_BF_ANGLE_P0     (0)
#define SYS_BF_ANGLE_P30    (1)
#define SYS_BF_ANGLE_P45    (2)
#define SYS_BF_ANGLE_P60    (3)
#define SYS_BF_ANGLE_P90    (4)
#define SYS_BF_ANGLE_P120   (5)
#define SYS_BF_ANGLE_P135   (6)
#define SYS_BF_ANGLE_P150   (7)
#define SYS_BF_ANGLE_P180   (8)
#define SYS_BF_ANGLE_M150   (9)
#define SYS_BF_ANGLE_M135   (10)
#define SYS_BF_ANGLE_M120   (11)
#define SYS_BF_ANGLE_M90    (12)
#define SYS_BF_ANGLE_M60    (13)
#define SYS_BF_ANGLE_M45    (14)
#define SYS_BF_ANGLE_M30    (15)
#define SYS_BF_ANGLE_LAST   SYS_BF_ANGLE_M30
#define SYS_BF_ANGLES_MAX   (SYS_BF_ANGLE_LAST+1)

#define SYS_BF_FILTER_LENGTH  24      /* must be even for optimized code */

extern Fract *sysBfFilters[];       /* table of all available filters */
extern tint  sysBfFilterAngles[];   /* table of filter angles in degrees */
#endif
/* nothing past this point */

