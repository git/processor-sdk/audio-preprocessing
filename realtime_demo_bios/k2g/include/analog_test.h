/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/**
 * \file      analog_test.h
 *
 * \brief     Common header file for audio analog test
 *
 */

#ifndef _ANALOG_TEST_H_
#define _ANALOG_TEST_H_

#include <cmb.h>
#include <evmc66x_i2c.h>

#include <xdc/std.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/IHeap.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/io/GIO.h>
#include <ti/sysbios/io/IOM.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sdo/edma3/drv/edma3_drv.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/family/c64p/Hwi.h>

#include <stdio.h>

#include <mcasp_drv.h>
#include <include/McaspLocal.h>

#include "cmb.h"
#include "audio_cfg.h"
#include "mcasp_cfg.h"
#include "edma_cfg.h"

/**
 *  \brief    Configures audio DAC module
 *
 *  \return    none
 */
void configAudioDAC(void);

/**
 *  \brief   Function to exit the test
 *
 *  \return    None
 */
void testRet(uint32_t status);

/**
 *  \brief    Function to exit the test
 *
 *  This function should be called by standalone diagnostic
 *  tests at the end of the test to return to diagnostic test framework
 *
 *  \param result [IN]  Test result
 *                      0 - Test Pass
 *                      1 - Test Fail
 *
 */
void testExit(int result);

#endif /* _ANALOG_TEST_H_ */
