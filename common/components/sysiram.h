/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* 
 *  sysiram.h: Internal memory heaps
*/
#ifndef _SYSIRAM_H
#define _SYSIRAM_H

#include <xdc/std.h>                /* required for types.h */
#include <types.h>

#define SYS_IRAM_ALIGN_LOG2 3
#define SYS_IRAM_ALIGN      (8*8/TYP_TWORD_SIZE)    /* alignment on 8 bytes */

#if SYS_IRAM_ALIGN != 8
#error unsuported word size
#endif

#define SYS_IRAM_PERMANENT  4000L   /* The number of 8-byte elements */
#define SYS_IRAM_SCRATCH    1000L

#define SYS_IRAM_PERMANENT_SIZE (SYS_IRAM_ALIGN*SYS_IRAM_PERMANENT)
#define SYS_IRAM_SCRATCH_SIZE   (SYS_IRAM_ALIGN*SYS_IRAM_SCRATCH)

extern __far tword sysIramPermanent[];
extern __far tword sysIramScratch[];

#endif
/* nothing past this point */

