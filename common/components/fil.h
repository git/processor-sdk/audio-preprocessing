/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/* 
 *  fil.h: Definitions for file handling functions (file stored in memory!)
*/
#ifndef _FIL_H
#define _FIL_H

#include <xdc/std.h>                /* required for types.h */
#include <types.h>

#include "sys.h"      /* for eampling rate, etc. */

/*======================================================================
 * File related constants
 *======================================================================*/

#define FIL_MAX         SYS_MICS_MAX    /* Maximum number of files */

#define FIL_L_ENDIAN    (FALSE)     /* Little endian byte ordering in file */
#define FIL_B_ENDIAN    (TRUE)1     /* Big endian byte ordering in file */

#define FIL_DURATION_MAX_MS     (60*1000L)                              /* 1min max duration */
#define FIL_LENGTH_MAX          (SYS_FS_HZ*FIL_DURATION_MAX_MS/1000)    /* max length in samples */
#define FIL_OUTDURATION_MAX_MS  (FIL_DURATION_MAX_MS+1*1000L)           /* 1s longer than input */
#define FIL_OUTLEN_MAX          (SYS_FS_HZ*FIL_OUTDURATION_MAX_MS/1000) /* in samples */

/*======================================================================
 * Bit masks, byte swapping macros, error codes
 *======================================================================*/

/* Bit masks for byte swapping */
#define FIL_LOW_BYTE    0x00FFu
#define FIL_HIGH_BYTE   0xFF00u

/* Macros for bit-mask manipulations */
#define FIL_GET_LOW(x)    ((x)&FIL_LOW_BYTE)
#define FIL_GET_HIGH(x)   (((x)>>8)&FIL_LOW_BYTE)
#define FIL_SWAP_BYTES(x) ((FIL_GET_LOW(x)<<8)|FIL_GET_HIGH(x))

/*======================================================================
 * File related types
 *======================================================================*/

/* File Configuration Structure */
struct filConfig_stc {
  tlong length;   /* number of samples in the input files */
  tint  nfiles;   /* number of memory input files to create */
  tbool big;      /* TRUE: big endian, FALSE: little endian byte order */
  tbool wrap;     /* TRUE: do wrap-around, FALSE: do not wrap-around */
  tlong outlen;   /* number of samples in the output file */
};
typedef struct filConfig_stc filConfig_t;

/* File Descriptor Structure (handles all files) */
struct filDescriptor_stc {
  tlong r_idx;    /* current read index (sample based) */
  tlong w_idx;    /* current write index (sample based) */
  tlong length;   /* number of samples in an input file (does not apply to output!) */
  tint  nfiles;   /* numbner of configured files */

  void  *filbase[FIL_MAX];    /* base addresses of file buffers */

  /* Use 'big' for both R&W */
  tbool big;      /* TRUE: big endian, FALSE: little endian byte order */
  tbool wrap;     /* TRUE: do wrap-around, FLASE: do not wrap-around (input only) */

  /* WRITE Part */
  tlong outlen;   /* Output buffer length in samples */
  void  *outbuf;  /* A pointer to the output buffer */
};
typedef struct filDescriptor_stc filDescriptor_t;

/*======================================================================
 * API Prototypes
 *======================================================================*/

extern int    filCreate(void**,filConfig_t*);         /* Initialize file descriptor */
extern void   *filGetHandle(void);                    /* Return file handle */
extern tlong  filLoad(char*, tlong, int);             /* Load file into memory */
extern int    filRead(void*,int,linSample*,int);      /* Read samples from files */
extern int    filWrite(void*, int, linSample*);       /* Write sample into file */

#endif
/* nothing past this point */

