/*
 * Copyright (c) 2016, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*=================================================================
 *  sys.c: Basic system functions.
 *=================================================================*/

#include <string.h>

#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>

#include <types.h>
#include <ti/mas/util/ecomem.h>
//#include <ti/mas/mhm/mhm.h>

#include "mhm/mhm.h"

#include "sys.h"
#include "syseram.h"
#include "sysiram.h"
#include "sysbfflt.h"

/* Globals */

/* Buffer descriptor array for memory allocations */
ecomemBuffer_t  sysCompBufs[SYS_COMP_MAXBUFS];

/* Input sample buffer (dual) (align=8) */
#pragma DATA_ALIGN(sysInBuf,8)
tword sysInBuf[SYS_IN_SIZE];

/* Virtual microphone frame outputs (align=8)*/
#pragma DATA_ALIGN(sysVmicFrame,8)
linSample sysVmicFrame[SYS_VMICS_MAX*SYS_FRAME_LENGTH];

/* Virtual microphone angles to be used (default is all 12, every 30 degrees) */
tint sysBfVMicAngles[SYS_VMICS_MAX] = {
  SYS_BF_ANGLE_P0,
  SYS_BF_ANGLE_P30,
  SYS_BF_ANGLE_P60,
  SYS_BF_ANGLE_P90,
  SYS_BF_ANGLE_P120,
  SYS_BF_ANGLE_P150,
  SYS_BF_ANGLE_P180,
  SYS_BF_ANGLE_M150,
  SYS_BF_ANGLE_M120,
  SYS_BF_ANGLE_M90,
  SYS_BF_ANGLE_M60,
  SYS_BF_ANGLE_M30
};

sysContext_t  sysContext;     /* Global System Context structure */

#define max(a,b)  (((a)>(b))?(a):(b))

/*=============================
 * Functions
 *=============================*/

/*=================================================================
 *  int sysAngleConfig(   Configure default virtual microphones
 *    tint nvmics)        - Number of virtual mics to configure
 *  
 *    This function accespts only 4,6,8,12 virtual microphones.
 *    If different number is used, manual configuration must be done
 *    with potentially providing different microphone filters for the array.
 *=================================================================*/

int sysAngleConfig(tint nvmics)
{
  int   err;

  err = SYS_ERR_SUCCESS;
  nvmics = sysContext.nvmics;
  switch (nvmics) {
  case 4:
    sysBfVMicAngles[0] = SYS_BF_ANGLE_P0;
    sysBfVMicAngles[1] = SYS_BF_ANGLE_P90;
    sysBfVMicAngles[2] = SYS_BF_ANGLE_P180;
    sysBfVMicAngles[3] = SYS_BF_ANGLE_M90;
    break;
  case 6:
    sysBfVMicAngles[0] = SYS_BF_ANGLE_P0;
    sysBfVMicAngles[1] = SYS_BF_ANGLE_P60;
    sysBfVMicAngles[2] = SYS_BF_ANGLE_P120;
    sysBfVMicAngles[3] = SYS_BF_ANGLE_P180;
    sysBfVMicAngles[4] = SYS_BF_ANGLE_M120;
    sysBfVMicAngles[5] = SYS_BF_ANGLE_M60;
    break;
  case 8:
    sysBfVMicAngles[0] = SYS_BF_ANGLE_P0;
    sysBfVMicAngles[1] = SYS_BF_ANGLE_P45;
    sysBfVMicAngles[2] = SYS_BF_ANGLE_P90;
    sysBfVMicAngles[3] = SYS_BF_ANGLE_P135;
    sysBfVMicAngles[4] = SYS_BF_ANGLE_P180;
    sysBfVMicAngles[5] = SYS_BF_ANGLE_M135;
    sysBfVMicAngles[6] = SYS_BF_ANGLE_M90;
    sysBfVMicAngles[7] = SYS_BF_ANGLE_M45;
    break;
  case 12:
    sysBfVMicAngles[0]  = SYS_BF_ANGLE_P0;
    sysBfVMicAngles[1]  = SYS_BF_ANGLE_P30;
    sysBfVMicAngles[2]  = SYS_BF_ANGLE_P60;
    sysBfVMicAngles[3]  = SYS_BF_ANGLE_P90;
    sysBfVMicAngles[4]  = SYS_BF_ANGLE_P120;
    sysBfVMicAngles[5]  = SYS_BF_ANGLE_P150;
    sysBfVMicAngles[6]  = SYS_BF_ANGLE_P180;
    sysBfVMicAngles[7]  = SYS_BF_ANGLE_M150;
    sysBfVMicAngles[8]  = SYS_BF_ANGLE_M120;
    sysBfVMicAngles[9]  = SYS_BF_ANGLE_M90;
    sysBfVMicAngles[10] = SYS_BF_ANGLE_M60;
    sysBfVMicAngles[11] = SYS_BF_ANGLE_M30;
    break;
  default:
    err = SYS_ERR_ANGLECONFIG;
    break;
  }
  return(err);
} /* sysAngleConfig */

/*=================================================================
 *  int sysCreate(
 *    sysConfig_t cfg);   - System configuration (see sys.h)
 *=================================================================*/

int sysCreate(sysConfig_t *cfg)
{
  int       k, err, nvmics;
  void      *heap_p;
  linSample *x_p;

  if (cfg->nmics < 1 || cfg->nmics > SYS_MICS_MAX ||
      cfg->nvmics < 1 || cfg->nvmics > SYS_VMICS_MAX) {
    return(SYS_ERR_BADCONFIG);
  }
  sysContext.nmics           = cfg->nmics;        /* Store # of mics used */
  nvmics = sysContext.nvmics = cfg->nvmics;       /* Store # of virtual mics used */
  sysContext.use_fileio      = cfg->use_fileio;   /* probably FALSE since slow */
  sysContext.eof             = FALSE;             /* no EOF reached yet */
  sysContext.use_default     = cfg->use_default;  /* TRUE for default vmic angles */
  for (k = 0; k < 3; k++) {
    sysContext.asnr_attn[k] = cfg->asnr_attn[k];
  }
  sysContext.asnr_delay   = cfg->asnr_delay;
  sysContext.asnr_enable  = cfg->asnr_enable;
  sysContext.vad_enable   = cfg->vad_enable;
  sysContext.drc_exp_knee = cfg->drc_exp_knee;
  sysContext.drc_max_amp  = cfg->drc_max_amp;
  sysContext.drc_enable   = cfg->drc_enable;

  /* Initialize input buffer pointers and read portion */
  sysContext.in_lo = &sysInBuf[0];                /* Input buffer start (Low part) */
  sysContext.in_hi = &sysInBuf[SYS_IN_SIZE/2];    /* High part of input buffer */
  sysContext.in_w  = sysContext.in_lo;            /* Write pointer */
  sysContext.in_r  = sysContext.in_hi;            /* Read pointer */
  memset(sysContext.in_r, 0xBA, SYS_IN_SIZE/2);   /* Initialize the read portion */

  /* Clear instance pointers */
  for (k = 0; k < SYS_VMICS_MAX; k++) {
    sysContext.bfInst_p[k] = sysContext.asnrInst_p[k] = NULL;
  }
  sysContext.mssInst_p = sysContext.vauInst_p = NULL;

  /* Initialize virtual microphone frame pointers */

  /* First check to see if default configurations are used */
  if (sysContext.use_default) {
    err = sysAngleConfig(nvmics);
    if (err != SYS_ERR_SUCCESS) {
      return(err);
    }
  }
  x_p = &sysVmicFrame[0];
  for (k = 0; k < nvmics; k++) {
    sysContext.vmicfrm[k]     = &x_p[k*SYS_FRAME_LENGTH];
    sysContext.vmicangles[k]  = sysBfVMicAngles[k];
  }
  for ( ; k < SYS_VMICS_MAX; k++) {
    sysContext.vmicfrm[k] = (linSample*)NULL;
    sysContext.vmicangles[k] = 0;
  }

  /* Create heaps */
  heap_p = mhmCreate(sysEramPermanent,SYS_ERAM_PERMANENT_SIZE,0);
  if (heap_p == NULL) {
    return(SYS_ERR_HEAPINIT);
  }
  sysContext.heapEP = heap_p;

  heap_p = mhmCreate(sysEramScratch,SYS_ERAM_SCRATCH_SIZE,0);
  if (heap_p == NULL) {
    return(SYS_ERR_HEAPINIT);
  }
  sysContext.heapES = heap_p;

  heap_p = mhmCreate(sysIramPermanent,SYS_IRAM_PERMANENT_SIZE,0);
  if (heap_p == NULL) {
    return(SYS_ERR_HEAPINIT);
  }
  sysContext.heapIP = heap_p;

  heap_p = mhmCreate(sysIramScratch,SYS_IRAM_SCRATCH_SIZE,0);
  if (heap_p == NULL) {
    return(SYS_ERR_HEAPINIT);
  }
  sysContext.heapIS = heap_p;

  return(SYS_ERR_SUCCESS);
} /* sysCreate */

/*=================================================================
 *  void sysError(  Print error and exit
 *    int err);     - System errror code (see sys.h)
 *=================================================================*/

void sysError(int err)
{
  System_printf("*** ERROR: %d\n", err);
  BIOS_exit(0);
} /* sysError */

/*=================================================================
 *  int sysHeapAlloc(     Allocate buffer from appropriate heap
 *    void *bd,             - buffer descriptor
 *    tint reset)           - 1: reset heap, 0: don't
 *=================================================================*/

int sysHeapAlloc(void *bd_in, tint reset)
{
  int     err = SYS_ERR_SUCCESS;
  tuint   size;
  void    *handle;
  tword   *base_w;
  tulong  *used_p;

  ecomemBuffer_t *bd_p = (ecomemBuffer_t*) bd_in;

  if (bd_p->mclass == ecomem_CLASS_EXTERNAL) {
    if (bd_p->volat) {
      handle = sysContext.heapES;
      used_p = &sysContext.heapES_used;
    }
    else {  /* permanent */
      handle = sysContext.heapEP;
      used_p = &sysContext.heapEP_used;
    }
  }
  else if (bd_p->mclass == ecomem_CLASS_INTERNAL) {
    if (bd_p->volat) {
      handle = sysContext.heapIS;
      used_p = &sysContext.heapIS_used;
    }
    else {  /* permanent */
      handle = sysContext.heapIP;
      used_p = &sysContext.heapIP_used;
    }
  }
  else {
    return(SYS_ERR_INVHEAPCLASS);
  }

  if (reset != 0) {
    mhmReset(handle);                 /* optionally reset the heap */
    *used_p = 0;                      /* reset used size to zero */
  }
  size = bd_p->size;
  base_w = (tword*)mhmAllocAligned(handle, size, bd_p->log2align);
  *used_p += size;          /* update the used size */

  bd_p->base = base_w;      /* store the bsae address for a buffer */

  if (base_w == NULL) {
    err = SYS_ERR_NOMEMORY;
  }
  return(err);
} /* sysHeapAlloc */

/*=================================================================
 *  int sysHeapAllocAll(  Allocate ALL buffers from appropriate heaps
 *    tint nbufs,           - number of buffers
 *    void *bdout,          - output buffer descriptors
 *    void *bdin)           - input buffer descriptors
 *=================================================================*/

int sysHeapAllocAll(tint nbufs, void *bdout_p, const void *bdin_p)
{
  int err;
  tint k, volatile_found;
  tulong es_used, is_used;    /* place to save scratch used sizes */
  ecomemBuffer_t  *bdout  = bdout_p;
  const ecomemBuffer_t  *bdin   = bdin_p;

  es_used = sysContext.heapES_used;     /* save in case scratch was reset */
  is_used = sysContext.heapIS_used;

  /* Allocate memory for all buffers */
  volatile_found = FALSE;
  for (k = 0; k < nbufs; k++) {
    bdout[k] = bdin[k];
    if (bdout[k].volat && !volatile_found) { /* first volatile? */
      err=sysHeapAlloc(&bdout[k], TRUE);          /* reset heap for the first volatile buffer */
      if (err < 0) {
        return(err);
      }
      volatile_found = TRUE;
    }
    else {  /* no need to reset heap */
      err=sysHeapAlloc(&bdout[k], FALSE);
      if (err < 0) {
        return(err);
      }
    }
  }

  /* fix the scratch heap used sizes if necessary */
  sysContext.heapES_used = max(es_used,sysContext.heapES_used);
  sysContext.heapIS_used = max(is_used,sysContext.heapIS_used);

  return(SYS_ERR_SUCCESS);
} /* sysHeapAllocAll */

/*==================================================*/
/* The following was based on MHM implementation!!! */
/*==================================================*/

/* Heap header that is placed at the beginning of a memory pool. */
typedef struct {
  tuint  total_size;   /* total number of words in a memory pool */
  tuint  used_size;    /* index of the first available word */
} sysMhmHead_t;
typedef tulong  sysMhmAddress_t;

/*=================================================================
 *  int sysPrintConfig(
 *    tuint scope);     - SYSM_SCOPE_STATIC: Static configuration like default constants
 *                        SYSM_SCOPE_DYNAMIC: Dynamic configuraiton like actual run-time values 
 *=================================================================*/

int sysPrintConfig(tuint scope)
{
  if (SYS_MASK_TEST(scope,SYSM_SCOPE_STATIC)) {
    System_printf("\nStatic information:\n");
    System_printf("tword: %d\n", sizeof(tword));
    System_printf("tint:  %d\n", sizeof(tint));
    System_printf("tlong: %d\n", sizeof(tlong));
    System_printf("Max #mics:  %d\n", SYS_MICS_MAX);
    System_printf("Max #vmics: %d\n", SYS_VMICS_MAX);
    System_printf("Fs: %d [Hz]\n", SYS_FS_HZ);
    System_printf("Frame duration: %d [ms]\n", SYS_FRAME_DURATION_MS);
    System_printf("Frame length: %ld samples\n", SYS_FRAME_LENGTH);
    System_printf("Frame size: %ld bytes\n", SYS_FRAME_SIZE);
    System_printf("Input buffer length: %ld samples\n", SYS_IN_LENGTH);
    System_printf("Frame buffer size: %ld bytes\n", SYS_IN_SIZE);
  }
  if (SYS_MASK_TEST(scope,SYSM_SCOPE_DYNAMIC)) {
    System_printf("\nDynamic information:\n");
    System_printf("#mics:  %d\n", sysContext.nmics);
    System_printf("#vmics: %d\n", sysContext.nvmics);
  }
  return(SYS_ERR_SUCCESS);
} /* sysPrintConfig */

/* nothing past this point */

