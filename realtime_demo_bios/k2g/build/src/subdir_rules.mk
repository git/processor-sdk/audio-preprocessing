################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Each subdirectory must supply rules for building sources it contributes
src/audio_cfg.obj: ../../../realtime_demo_bios/k2g/src/audio_cfg.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/audio_cfg.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/edma_cfg.obj: ../../../realtime_demo_bios/k2g/src/edma_cfg.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/edma_cfg.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/main.obj: ../../../realtime_demo_bios/k2g/src/main.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/main.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/mcasp_cfg.obj: ../../../realtime_demo_bios/k2g/src/mcasp_cfg.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/mcasp_cfg.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sample_cs.obj: ../../../realtime_demo_bios/k2g/src/sample_cs.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/sample_cs.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sample_k2g_cfg.obj: ../../../realtime_demo_bios/k2g/src/sample_k2g_cfg.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/sample_k2g_cfg.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/sample_k2g_int_reg.obj: ../../../realtime_demo_bios/k2g/src/sample_k2g_int_reg.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/sample_k2g_int_reg.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/test_exit.obj: ../../../realtime_demo_bios/k2g/src/test_exit.c $(GEN_OPTS) $(GEN_HDRS)
	@echo 'Building file: $<'
	@echo 'Invoking: C6000 Compiler'
	"${C6X_GEN_INSTALL_PATH}/bin/cl6x" -mv6600 --abi=eabi --opt_for_speed=5 --include_path="${EDMA3LLD_BIOS6_INSTALLDIR}/packages" --include_path="${C6X_GEN_INSTALL_PATH}/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb/src/evmK2G/include" --include_path="../../../realtime_demo_bios/k2g/include" --include_path="../../../common/components" --include_path="${PDK_INSTALL_PATH}/ti/drv/mcasp" --include_path="${PDK_INSTALL_PATH}/ti/board" --include_path="${PDK_INSTALL_PATH}/ti/board/src/evmK2G/include" --include_path="${PDK_INSTALL_PATH}/ti/addon/cmb" --include_path="${PDK_INSTALL_PATH}" --include_path="${PDK_INSTALL_PATH}/ti/csl" -g --define=DEVICE_K2G --define=SOC_K2G --define=evmK2G --define=PLATFORM_INIT_IN --define=BUILD_DSP --diag_warning=225 --diag_wrap=off --display_error_number --preproc_with_compile --preproc_dependency="src/test_exit.d" --obj_directory="src" $(GEN_OPTS__FLAG) "$<"
	@echo 'Finished building: $<'
	@echo ' '


