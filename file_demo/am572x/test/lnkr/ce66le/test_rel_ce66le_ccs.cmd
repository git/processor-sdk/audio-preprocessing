
-c  /* ROM autoinitialization module */
-a  /* LINK USING C CONVENTIONS      */

-l ../../../../../../../aer_c64Px_obj_17_0_0_0_mod/packages/ti/mas/aer/lib/aer_a.ae64P
-l ../../../../../../../aer_c64Px_obj_17_0_0_0_mod/packages/ti/mas/aer/lib/aer_c.ae64P
-l ../../../../../../../volib_C66_2_1_0_1/packages/ti/mas/vpe/lib/vpe_c.ae66
-l ../../../../../../../volib_C66_2_1_0_1/packages/ti/mas/vpe/lib/vpe_a.ae66
-l ../../../../../../../volib_C66_2_1_0_1/packages/ti/mas/vau/lib/vau_c.ae66
-l ../../../../../../../volib_C66_2_1_0_1/packages/ti/mas/sdk/lib/sdk_c.ae66
-l ../../../../../../../volib_C66_2_1_0_1/packages/ti/mas/util/lib/util_c.ae66
-l ../../../../../../../volib_C66_2_1_0_1/packages/ti/mas/util/lib/util_a.ae66

-stack 0x1000
-heap  0x1000

MEMORY
{
  IVECS   : origin = 0x800000, length = 0x000220
  L2_SRAM : o = 0x00801000 l = 0x0003F000   /* 256kB - 4kB L2 cache */
  DDR: origin = 0x80000000, length = 0x10000000 /* 256M external */
}

SECTIONS
{
  .text   > L2_SRAM
  .cinit  > L2_SRAM
  .switch > L2_SRAM
 
  .bss    > L2_SRAM
  .const  > L2_SRAM
  .sysmem > L2_SRAM
  .stack  > L2_SRAM
  .data   > L2_SRAM
  .cio    > L2_SRAM
  .far    > L2_SRAM

  .fardata   > L2_SRAM
  .neardata  > L2_SRAM
  .rodata    > L2_SRAM

  /* Heap used by simulation */
  .simHeap > L2_SRAM

  /* file IO buffers */
  .fileInBufs > DDR
  .fileOutBufs > DDR

  .volatileMemBufs > L2_SRAM
  .nonVolatileMemBufs > L2_SRAM

  /* debug buffer */
  VPEExtDbg > DDR
}
 
