#*******************************************************************************
#* FILE PURPOSE: Top level makefile for Creating Kernel CCS Projects
#*******************************************************************************
#* FILE NAME: makefile
#*
#* DESCRIPTION: Defines Compiler tools paths, CCS path and rule to create and build CCS projects 
#*
#*
#* This is an auto-generated file			  
#*******************************************************************************
#*
# (Mandatory) Specify where various tools are installed.

# CCSCGTVER	  - C6x Code Generation version to be used for building CCS project
export CCSCGTVER ?= 7.4.2

# CCSCGTVERC55	 - C55x Code Generation version to be used for building CCS project
export CCSCGTVERC55 ?= 4.4.1

# CCS_INSTALL_DIR	  - TI Code Composer Studio install directory
export CCSV5_INSTALL_DIR ?= C:/Program Files/Texas Instruments/ccsv5

ifneq ($(findstring $(CCSVERSION), CCSV5),)
	export CCS_INSTALL_DIR ?= $(CCSV5_INSTALL_DIR)
	export ECLIPSE_CMD ?= "$(CCS_INSTALL_DIR)/eclipse/eclipsec.exe" -noSplash
	export CDTFILE ?= .cproject
endif

# PATH TO  MAS INSTALL DIR
export MAS_INSTALL_DIR		?= C:/MAS_Tools

# Common Macros used in make

ifndef RMDIR
export RMDIR = DEL /Q /F /S
endif

ifneq ($(findstring $(SHELL), sh.exe),)
	quote = "
	FIXCCSPJT=
else
	quote = '
	FIXCCSPJT=${XDC_INSTALL_DIR}/xs -f ../../swtools/fixccspjt.js $(@D)/$(CDTFILE)
endif

# PHONY Targets
.PHONY: all clean cleanall .projects 

# FORCE Targets
FORCE: 

# all rule
all: .executables
.executables: .projects
.projects:
.xdcenv.mak:
Package.mak:
package.bld:

#############################################################

# Rule to Build Project test_rel_c64Ple_C64PLE_LE_COFF

#############################################################

.executables: ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out

.projects: ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/$(CDTFILE)

ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/$(CDTFILE): .xdcenv.mak Package.bld Package.mak
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectCreate -ccs.name test_rel_c64Ple_C64PLE_LE_COFF -ccs.device com.ti.ccstudio.deviceModel.C6000.CustomC6000Device -ccs.endianness little -ccs.kind com.ti.ccstudio.managedbuild.core.ProjectKind_Executable -ccs.outputType executable -ccs.linkFile EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_LNKRC64PLE/test_rel_c64Ple_ccs.cmd -ccs.linkFile EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_SRC/vpesim.c -ccs.linkFile EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_SRC/nrsim.c -ccs.linkFile EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_SRC/nrbufs.c -ccs.linkFile EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_SRC/vpesim_setup.c -ccs.setCompilerOptions "-mv64plus -k -mw -Dti_targets_C64P -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw  -g -mi10000 -as -ss   -ms3 -pg -g  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/c64Ple -i../../../src" @configurations Debug -ccs.setCompilerOptions "-mv64plus -k -mw -Dti_targets_C64P -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw -os -g -mi10000 -as -ss -o3 --optimize_with_debug -ms3 -pg  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/c64Ple -i../../../src" @configurations Release -ccs.location ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF -ccs.overwrite full -ccs.rts libc.a -ccs.cgtVersion $(CCSCGTVER) -ccs.outputFormat coff -ccs.definePathVariable EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_LNKRC64PLE "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/lnkr/c64Ple" -ccs.definePathVariable EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_SRC "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/src"
	${FIXCCSPJT}
	echo XDC_CG_ROOT=C:/MAS_Tools/xdctools_3_24_06_63 >> ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/macros.ini
	echo XDAIS_CG_ROOT=C:/MAS_Tools/xdais_7_23_00_06 >> ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/macros.ini
	echo EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_LNKRC64PLE=../../lnkr/c64Ple >> ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/macros.ini
	echo EXT_ROOT__TEST_REL_C64PLE_C64PLE_LE_COFF_SRC=../../src >> ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/macros.ini
	-$(RMDIR) "C:/Temp/workspace"
ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out: lnkr/c64Ple/test_rel_c64Ple_ccs.cmd
ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out: src/vpesim.c
ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out: src/nrsim.c
ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out: src/nrbufs.c
ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out: src/vpesim_setup.c
ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out: ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/.project
	-@ echo Importing Project test_rel_c64Ple_C64PLE_LE_COFF into workspace ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectImport -ccs.projects test_rel_c64Ple_C64PLE_LE_COFF -ccs.location "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF"
	-@ echo Building Project test_rel_c64Ple_C64PLE_LE_COFF ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectBuild -ccs.projects test_rel_c64Ple_C64PLE_LE_COFF -ccs.configuration Debug
	-@ echo Project test_rel_c64Ple_C64PLE_LE_COFF Completed ...
	-@ echo ########################################
	-$(RMDIR) "C:/Temp/workspace"
	if test ! -f ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF/Debug/test_rel_c64Ple_C64PLE_LE_COFF.out; then exit 1; fi;

clean::
	-$(RMDIR) "ccsProjects/test_rel_c64Ple_C64PLE_LE_COFF"


#############################################################

# Rule to Build Project test_rel_c64Pbe_C64PBE_BE_COFF

#############################################################

.executables: ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out

.projects: ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/$(CDTFILE)

ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/$(CDTFILE): .xdcenv.mak Package.bld Package.mak
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectCreate -ccs.name test_rel_c64Pbe_C64PBE_BE_COFF -ccs.device com.ti.ccstudio.deviceModel.C6000.CustomC6000Device -ccs.endianness big -ccs.kind com.ti.ccstudio.managedbuild.core.ProjectKind_Executable -ccs.outputType executable -ccs.linkFile EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_LNKRC64PBE/test_rel_c64Pbe_ccs.cmd -ccs.linkFile EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_SRC/vpesim.c -ccs.linkFile EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_SRC/nrsim.c -ccs.linkFile EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_SRC/nrbufs.c -ccs.linkFile EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_SRC/vpesim_setup.c -ccs.setCompilerOptions "-mv64plus -k -mw  -Dti_targets_C64P_big_endian -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw  -g -mi10000 -as -ss   -ms3 -pg -g  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/c64Pbe -i../../../src" @configurations Debug -ccs.setCompilerOptions "-mv64plus -k -mw  -Dti_targets_C64P_big_endian -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw -os -g -mi10000 -as -ss -o3 --optimize_with_debug -ms3 -pg  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/c64Pbe -i../../../src" @configurations Release -ccs.location ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF -ccs.overwrite full -ccs.rts libc.a -ccs.cgtVersion $(CCSCGTVER) -ccs.outputFormat coff -ccs.definePathVariable EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_LNKRC64PBE "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/lnkr/c64Pbe" -ccs.definePathVariable EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_SRC "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/src"
	${FIXCCSPJT}
	echo XDC_CG_ROOT=C:/MAS_Tools/xdctools_3_24_06_63 >> ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/macros.ini
	echo XDAIS_CG_ROOT=C:/MAS_Tools/xdais_7_23_00_06 >> ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/macros.ini
	echo EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_LNKRC64PBE=../../lnkr/c64Pbe >> ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/macros.ini
	echo EXT_ROOT__TEST_REL_C64PBE_C64PBE_BE_COFF_SRC=../../src >> ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/macros.ini
	-$(RMDIR) "C:/Temp/workspace"
ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out: lnkr/c64Pbe/test_rel_c64Pbe_ccs.cmd
ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out: src/vpesim.c
ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out: src/nrsim.c
ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out: src/nrbufs.c
ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out: src/vpesim_setup.c
ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out: ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/.project
	-@ echo Importing Project test_rel_c64Pbe_C64PBE_BE_COFF into workspace ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectImport -ccs.projects test_rel_c64Pbe_C64PBE_BE_COFF -ccs.location "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF"
	-@ echo Building Project test_rel_c64Pbe_C64PBE_BE_COFF ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectBuild -ccs.projects test_rel_c64Pbe_C64PBE_BE_COFF -ccs.configuration Debug
	-@ echo Project test_rel_c64Pbe_C64PBE_BE_COFF Completed ...
	-@ echo ########################################
	-$(RMDIR) "C:/Temp/workspace"
	if test ! -f ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF/Debug/test_rel_c64Pbe_C64PBE_BE_COFF.out; then exit 1; fi;

clean::
	-$(RMDIR) "ccsProjects/test_rel_c64Pbe_C64PBE_BE_COFF"


#############################################################

# Rule to Build Project test_rel_ce64Ple_CE64PLE_LE_ELF

#############################################################

.executables: ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out

.projects: ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/$(CDTFILE)

ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/$(CDTFILE): .xdcenv.mak Package.bld Package.mak
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectCreate -ccs.name test_rel_ce64Ple_CE64PLE_LE_ELF -ccs.device com.ti.ccstudio.deviceModel.C6000.CustomC6000Device -ccs.endianness little -ccs.kind com.ti.ccstudio.managedbuild.core.ProjectKind_Executable -ccs.outputType executable -ccs.linkFile EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_LNKRCE64PLE/test_rel_ce64Ple_ccs.cmd -ccs.linkFile EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_SRC/vpesim.c -ccs.linkFile EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_SRC/nrsim.c -ccs.linkFile EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_SRC/nrbufs.c -ccs.linkFile EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_SRC/vpesim_setup.c -ccs.setCompilerOptions "-mv64plus -k -mw --strip_coff_underscore -Dti_targets_elf_C64P -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw  -g -mi10000 -as -ss   -ms3 -pg -g  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/ce64Ple -i../../../src" @configurations Debug -ccs.setCompilerOptions "-mv64plus -k -mw --strip_coff_underscore -Dti_targets_elf_C64P -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw -os -g -mi10000 -as -ss -o3 --optimize_with_debug -ms3 -pg  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/ce64Ple -i../../../src" @configurations Release -ccs.location ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF -ccs.overwrite full -ccs.rts libc.a -ccs.cgtVersion $(CCSCGTVER) -ccs.outputFormat elf -ccs.definePathVariable EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_LNKRCE64PLE "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/lnkr/ce64Ple" -ccs.definePathVariable EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_SRC "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/src"
	${FIXCCSPJT}
	echo XDC_CG_ROOT=C:/MAS_Tools/xdctools_3_24_06_63 >> ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/macros.ini
	echo XDAIS_CG_ROOT=C:/MAS_Tools/xdais_7_23_00_06 >> ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/macros.ini
	echo EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_LNKRCE64PLE=../../lnkr/ce64Ple >> ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/macros.ini
	echo EXT_ROOT__TEST_REL_CE64PLE_CE64PLE_LE_ELF_SRC=../../src >> ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/macros.ini
	-$(RMDIR) "C:/Temp/workspace"
ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out: lnkr/ce64Ple/test_rel_ce64Ple_ccs.cmd
ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out: src/vpesim.c
ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out: src/nrsim.c
ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out: src/nrbufs.c
ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out: src/vpesim_setup.c
ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out: ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/.project
	-@ echo Importing Project test_rel_ce64Ple_CE64PLE_LE_ELF into workspace ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectImport -ccs.projects test_rel_ce64Ple_CE64PLE_LE_ELF -ccs.location "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF"
	-@ echo Building Project test_rel_ce64Ple_CE64PLE_LE_ELF ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectBuild -ccs.projects test_rel_ce64Ple_CE64PLE_LE_ELF -ccs.configuration Debug
	-@ echo Project test_rel_ce64Ple_CE64PLE_LE_ELF Completed ...
	-@ echo ########################################
	-$(RMDIR) "C:/Temp/workspace"
	if test ! -f ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF/Debug/test_rel_ce64Ple_CE64PLE_LE_ELF.out; then exit 1; fi;

clean::
	-$(RMDIR) "ccsProjects/test_rel_ce64Ple_CE64PLE_LE_ELF"


#############################################################

# Rule to Build Project test_rel_ce64Pbe_CE64PBE_BE_ELF

#############################################################

.executables: ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out

.projects: ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/$(CDTFILE)

ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/$(CDTFILE): .xdcenv.mak Package.bld Package.mak
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectCreate -ccs.name test_rel_ce64Pbe_CE64PBE_BE_ELF -ccs.device com.ti.ccstudio.deviceModel.C6000.CustomC6000Device -ccs.endianness big -ccs.kind com.ti.ccstudio.managedbuild.core.ProjectKind_Executable -ccs.outputType executable -ccs.linkFile EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_LNKRCE64PBE/test_rel_ce64Pbe_ccs.cmd -ccs.linkFile EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_SRC/vpesim.c -ccs.linkFile EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_SRC/nrsim.c -ccs.linkFile EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_SRC/nrbufs.c -ccs.linkFile EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_SRC/vpesim_setup.c -ccs.setCompilerOptions "-mv64plus -k -mw --strip_coff_underscore -Dti_targets_elf_C64P_big_endian -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw  -g -mi10000 -as -ss   -ms3 -pg -g  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/ce64Pbe -i../../../src" @configurations Debug -ccs.setCompilerOptions "-mv64plus -k -mw --strip_coff_underscore -Dti_targets_elf_C64P_big_endian -Dxdc_target_types__=ti/targets/std.h  -D__CCS_PROJECT_REL_PATH  -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw -os -g -mi10000 -as -ss -o3 --optimize_with_debug -ms3 -pg  -i../../../../../../../ -i$$$${XDC_CG_ROOT}/packages  -i$$$${XDAIS_CG_ROOT}/packages -i../../../lnkr/ce64Pbe -i../../../src" @configurations Release -ccs.location ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF -ccs.overwrite full -ccs.rts libc.a -ccs.cgtVersion $(CCSCGTVER) -ccs.outputFormat elf -ccs.definePathVariable EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_LNKRCE64PBE "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/lnkr/ce64Pbe" -ccs.definePathVariable EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_SRC "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/src"
	${FIXCCSPJT}
	echo XDC_CG_ROOT=C:/MAS_Tools/xdctools_3_24_06_63 >> ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/macros.ini
	echo XDAIS_CG_ROOT=C:/MAS_Tools/xdais_7_23_00_06 >> ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/macros.ini
	echo EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_LNKRCE64PBE=../../lnkr/ce64Pbe >> ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/macros.ini
	echo EXT_ROOT__TEST_REL_CE64PBE_CE64PBE_BE_ELF_SRC=../../src >> ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/macros.ini
	-$(RMDIR) "C:/Temp/workspace"
ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out: lnkr/ce64Pbe/test_rel_ce64Pbe_ccs.cmd
ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out: src/vpesim.c
ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out: src/nrsim.c
ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out: src/nrbufs.c
ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out: src/vpesim_setup.c
ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out: ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/.project
	-@ echo Importing Project test_rel_ce64Pbe_CE64PBE_BE_ELF into workspace ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectImport -ccs.projects test_rel_ce64Pbe_CE64PBE_BE_ELF -ccs.location "M:/TI_MAS_VOLIB_2_1_0_1_bldforge/dsps_gtmas/ti/mas/vpe/test/ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF"
	-@ echo Building Project test_rel_ce64Pbe_CE64PBE_BE_ELF ...
	$(ECLIPSE_CMD)  -data "C:/Temp/workspace" -application com.ti.ccstudio.apps.projectBuild -ccs.projects test_rel_ce64Pbe_CE64PBE_BE_ELF -ccs.configuration Debug
	-@ echo Project test_rel_ce64Pbe_CE64PBE_BE_ELF Completed ...
	-@ echo ########################################
	-$(RMDIR) "C:/Temp/workspace"
	if test ! -f ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF/Debug/test_rel_ce64Pbe_CE64PBE_BE_ELF.out; then exit 1; fi;

clean::
	-$(RMDIR) "ccsProjects/test_rel_ce64Pbe_CE64PBE_BE_ELF"

