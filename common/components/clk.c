/*
 * Copyright (c) 2017, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

/*=================================================================
 *  clk.c: Clock functions, SWI's, Tx Task
 *=================================================================*/

#include <strings.h>

#include "fil.h"                      /* FILE I/O implementation */
#include "sys.h"                      /* System API and structures */
#include "sysbfflt.h"                 /* System support for BF filters */

/* The infamous xdc/std.h must come before any header file which uses XDC symbols */
#include <xdc/std.h>              /* mandatory - have to include first, for BIOS types */
#include <types.h>
#include <ti/mas/aer/bf.h>
//#include <ti/mas/aer/mss.h>
#include "mss/mss.h"              /* local version used */
#include <ti/mas/vpe/asnr.h>
#if (SYS_USE_DRC)
///#include <ti/mas/vau/vau.h>
#include <ti/mas/aer/drc.h>
#endif

/*----------------------------------------
 *  BIOS header files
 *----------------------------------------*/
#include <ti/sysbios/BIOS.h>          /* mandatory - if you call APIs like BIOS_start() */
#include <xdc/cfg/global.h>           /* header file for statically defined objects/handles */
#include <xdc/runtime/System.h>       /* for System_printf, and similar */
#include <xdc/runtime/Timestamp.h>    /* for benchmarking/profiling */

#include <xdc/runtime/Log.h>          /* for tracing */
#include <xdc/runtime/Diags.h>
#include <ti/uia/events/UIAEvt.h>     /* and more tracing */

#include <ti/sysbios/knl/Semaphore.h> /* this looks obvious */

#define Timestamp_get Timestamp_get32 /* use 32-bit time stamps */

#define MIN(a,b)    (((a)>(b))?(b):(a))     /* Min/Max macros */
#define MAX(a,b)    (((a)<(b))?(b):(a))
#define loop        while(1)                /* endless loop for the task */

/*=============================
 * Functions
 *=============================*/

/*=================================================================
 *  void clkDataIn(void)
 *
 *  This is a clock function that would start SWI to read input data.
 *  In this case from a file/memory. It is statically configured in BIOS.
 *=================================================================*/

void clkDataIn(void)
{
  Swi_post(swidatainhandle);
} /* clkDataIn */

/*===========================================
 *  SWI for getting data in from file/memory or A/D
 *
 *  Uses filRead() API to read frame of samples from "memory file".
 *  Swaps the read/write pointers so that the system can read new data.
 *  It wakes up the txTask to do that. It is statically configured in BIOS.
 *
 *===========================================*/

/* A few debug stats */
tuint  swiDataIn_errcnt = 0uL;      /* count errors reading data */
tuint  swiDataIn_overflow = 0uL;    /* count how many times r/w pointers were the same */
tint   swiDataIn_last_error;        /* last error code */

void swiDataIn(void)
{
  tint err;
  void *fid, *in_w, *in_r;

  in_w = sysContext.in_w;     /* Fetch the read and write pointers for input buffer */
  in_r = sysContext.in_r;
  if (in_r == in_w) {         /* this should never happen */
    swiDataIn_overflow++;
    return;
  }

  /* Assume the write pointer is valid and use it to read the frame in */
  fid = filGetHandle();
  err = filRead(fid, SYS_FRAME_LENGTH, in_w, SYS_FRAME_LENGTH);
  if (err != SYS_ERR_SUCCESS) {
    swiDataIn_errcnt++;
    swiDataIn_last_error = err;
    return;
  }
  else {  /* Swap the r/w pointers */
    sysContext.in_r = in_w;   /* we can do this because this SWI is at higher priority */
    sysContext.in_w = in_r;   /* than the Tx Task and we know that r/w are different */
    /* We assume that the Tx Task already finished using the read pointer */
    /* So, next time we come we would write into the "other" buffer. */
  }

  /* Here we need to post a semaphore to wake up the Tx task */
  Semaphore_post(semTxWakeUp);

} /* swiDataIn */

/*===========================================
 *  Tx Task
 *
 *  It wakes up on semaphore (semTxWakeUp).
 *  It performs the following tasks:
 *    - give next frame to the BF's
 *    - Run ASNR at the output of each BF to reduce noise further
 *    - Provide all virtual mic outputs to the MSS for selection
 *    - Pass output of MSS through VAU for speech activity detection
 *    - Write the output signal to the output buffer/file or D/A
 *    - Profile execution of all major modules
 *    - Trace beam selection and VAU activity
 *
 *===========================================*/

#if (SYS_USE_DRC)
/* Output frame for MSS, input for DRC */
#pragma DATA_ALIGN(txOutFrame1,8)
linSample txOutFrame1[SYS_FRAME_LENGTH];

/* Output frame for DRC, input for VAU */
#pragma DATA_ALIGN(txOutFrame2,8)
linSample txOutFrame2[SYS_FRAME_LENGTH];
#else
/* Output frame for MSS, input for VAU */
#pragma DATA_ALIGN(txOutFrame,8)
linSample txOutFrame[SYS_FRAME_LENGTH];
#endif

typedef struct txBfDebug_stc {
 tulong frmcnt;     /* normal frames */
 tulong silcnt;     /* silence frames */
 tuint  invsrc;     /* no mic active, invalid output */
 tuint  invopt;     /* >1 mic active, invalid output */
} txBfDebug_t;

typedef struct txTaskDebug_stc {
  tuint overrun;                    /* counts how many times we ran out of MIPS */
  txBfDebug_t bf[SYS_VMICS_MAX];    /* beamformer statistics */
} txTaskDebug_t;

txTaskDebug_t txTaskDebug;      /* Tx task debug stats */

/* Profiling/benchmarking information for the Tx task */
typedef struct txTaskProfileData_stc {
  tulong  min;              /* Minimum number of cycles */
  tulong  max;              /* Maximum number of cycles */
  tulong  n;                /* Number of measurements */
  float   total;            /* Total number of cycles */
} txTaskProfileData_t;

typedef struct txTaskProfile_stc {
  txTaskProfileData_t   bf;       /* Beamformer profile */
  txTaskProfileData_t   asnr;     /* ASNR profile */
  txTaskProfileData_t   mss;      /* MSS profile */
  txTaskProfileData_t   drc;      /* DRC profile */
  txTaskProfileData_t   vau;      /* VAU profile */
} txTaskProfile_t;
volatile txTaskProfile_t  txTaskProfile = {
  {~(0uL), 0, 0, 0.0f},
  {~(0uL), 0, 0, 0.0f},
  {~(0uL), 0, 0, 0.0f},
  {~(0uL), 0, 0, 0.0f},
  {~(0uL), 0, 0, 0.0f}
};

/* To be used for debug trace */
mssSrc_t    mssDbgCurSrc = {
  -1, -1                        /* Current source group/index */
};
mssSrc_t    mssDbgNewSrc = {
  -1, -1                        /* New source group/index */
};

/* Tx Task main routine */
void taskTx(void)
{
  Int       semcnt;     /* count from a counting semaphore */
  int       k;          /* loop counter */
  tint      nmics, nvmics, err, angle, alert, oldalert = -1;

  volatile tulong t1, t2;       /* for profiling */
  tulong          delta;

  void      *inst_p, *fid;
  linSample *in_r;                      /* pointer to current microphone input buffer */
  linSample *frame_p;                   /* pointer to signal frame */
  linSample *outframe_p;                /* Output frame pointer for VAU */
  linSample *mics_in[SYS_MICS_MAX];     /* pointers to microphone inputs */

  mssDebugStat_t  mssDbg;

#if 0
  ISPHENC1_FrameType vauOldFrameType;       /* previous speech/noise */
  ISPHENC1_FrameType vauFrameType;          /* use for every frame */

  vauOldFrameType = (ISPHENC1_FrameType)255;    /* Start with invalid, update only on changes */
#endif

  memset(&txTaskDebug,0,sizeof(txTaskDebug));   /* zero debug stats */

  Log_print0(Diags_USER1,"taskTxStart");        /* Timestamp the beginning of the task */

  fid = filGetHandle();
  loop {

    /*-------------------------------------------------------------------------------------*/
    Semaphore_pend(semTxWakeUp, BIOS_WAIT_FOREVER);   /* wait for swiDataIn to wake you up */
    /*-------------------------------------------------------------------------------------*/

    /* In a real system, it could make sense to wait with time-out and post error condition
     *    if the time-out expires. For example, if the frame duration is 10ms, we could wait
     *    for 15ms or longer. */

    semcnt = Semaphore_getCount(semTxWakeUp);
    if (semcnt > 0) {
      txTaskDebug.overrun++;  /* indicate we ran out of MIPS since we were asked to be waken up
                               * multiple times before getting here. There were more events pending.
                               * We are dropping a frame of input data here. */
      continue;               /* Skip this pass through the loop to catch up to the last pending event */
    }
    nmics = sysContext.nmics;                   /* fetch number of mics */
    in_r  = (linSample *)sysContext.in_r;       /* this was written by swiDataIn */
    for (k = 0; k < nmics; k++) {
      mics_in[k] = &in_r[k*SYS_FRAME_LENGTH];   /* find the frame start for each microphone */
    }
    /* consume samples pointed to by read pointer in_r as provided in misc_in[] */

    /* Here comes a lot of work */
    /* We start with beamformers */

    nvmics = sysContext.nvmics;
    t1 = Timestamp_get();
    for (k = 0; k < nvmics; k++) {
      inst_p  = sysContext.bfInst_p[k];     /* fetch the bf instance pointer */
      frame_p = sysContext.vmicfrm[k];      /* point to the output frame buffer */

      err = bfProcess(inst_p, (void*)&mics_in[0], (void*)frame_p);

      if (err == bf_NOERR) {
        txTaskDebug.bf[k].frmcnt++;             /* Record some debug info */
      }
      else if (err == bf_ERR_NOTOPENED) {
        SYS_CHECK_ERROR(SYS_ERR_BFERROR);
      }
      else if (err == bf_ERR_DISABLED) {
        txTaskDebug.bf[k].silcnt++;
      }
      else if (err == bf_ERR_INVALIDSRC) {
        txTaskDebug.bf[k].invsrc = TRUE;
      }
      else if (err == bf_ERR_INVALIDOPT) {
        txTaskDebug.bf[k].invopt = TRUE;
      }
      else {
        SYS_CHECK_ERROR(SYS_ERR_BFERROR);
      } /* if */
    } /* for */
    t2 = Timestamp_get();
    delta = t2-t1;
    txTaskProfile.bf.min = MIN(txTaskProfile.bf.min,delta);
    txTaskProfile.bf.max = MAX(txTaskProfile.bf.max,delta);
    txTaskProfile.bf.n++;
    txTaskProfile.bf.total += (float)delta;

    /* At this point we have consumed all input samples. Currently we did not implement
     * any protection to prevent the swiDataIn from stepping over while we were doing this.
     * We could let this task to handle the read pointer and SWI to handle write pointer which 
     * could be used to detect if such overrun would happen. */

    /* Done with the beamformers */
    /* Start ASNR's */

    t1 = Timestamp_get();
    for (k = 0; k < nvmics; k++) {
      inst_p  = sysContext.asnrInst_p[k];   /* fetch the bf instance pointer */
      frame_p = sysContext.vmicfrm[k];      /* point to the output frame buffer */

      err = asnrProcess(inst_p, (void*)frame_p, (void*)frame_p);

      if (err != asnr_NOERR) {
        SYS_CHECK_ERROR(SYS_ERR_ASNRERROR);
      } /* if */
    } /* for */
    t2 = Timestamp_get();
    delta = t2-t1;
    txTaskProfile.asnr.min = MIN(txTaskProfile.asnr.min,delta);
    txTaskProfile.asnr.max = MAX(txTaskProfile.asnr.max,delta);
    txTaskProfile.asnr.n++;
    txTaskProfile.asnr.total += (float)delta;

    /* Done with the ASNR's */
    /* Run MSS */

    t1 = Timestamp_get();
    inst_p  = sysContext.mssInst_p;         /* fetch the MSS instance pointer */
#if (SYS_USE_DRC)
	    frame_p = txOutFrame1;              /* point to the output frame buffer */
#else
    frame_p = txOutFrame;                   /* point to the output frame buffer */
#endif

    err = mssProcess(inst_p, (void*)frame_p,      /* instance and output frame pointers */
                     (void*)frame_p,              /* WORKAROUND (not used, but no NULL) */
                     (void**)sysContext.vmicfrm,  /* Virtual microphones (beams) */
                     NULL,                        /* No remote mics */
                     NULL,                        /* No clean mics */
                     (void**)mics_in,             /* Raw microphone array inputs */
                     NULL);                       /* Beam not supported (see fixed inputs) */

    if (err != mss_NOERR) {
      SYS_CHECK_ERROR(SYS_ERR_MSSERROR);
    } /* if */
    t2 = Timestamp_get();
    delta = t2-t1;
    txTaskProfile.mss.min = MIN(txTaskProfile.mss.min,delta);
    txTaskProfile.mss.max = MAX(txTaskProfile.mss.max,delta);
    txTaskProfile.mss.n++;
    txTaskProfile.mss.total += (float)delta;

    /* Trace source selection */
    /*    Write Args:
     *      arg2: (value) Angle in degrees
     *      arg3: (aux1)  0 - current source, 1 - new source
     *      arg4: (aux2)  source index
     */
    err = mssDebugStat(inst_p, &mssDbg);
    if (err !=mss_NOERR) {
      SYS_CHECK_ERROR(SYS_ERR_MSSDEBUG);
    }
    /* mssDbg.cur_src.group/.index has the current source */
    /* mssDbg.new_src.group/.index has "proposed" source */
    if (mssDbg.cur_src.group != mssDbgCurSrc.group ||
        mssDbg.cur_src.index != mssDbgCurSrc.index)
    {
      mssDbgCurSrc = mssDbg.cur_src;
      angle = sysBfFilterAngles[sysBfVMicAngles[mssDbgCurSrc.index]];
      Log_write6(UIAEvt_intWithKey, angle, 0, mssDbgCurSrc.index, (IArg)"MSS-C: %d, G:%d", 0, mssDbgCurSrc.group);
    }
    if (mssDbg.new_src.group != mssDbgNewSrc.group ||
        mssDbg.new_src.index != mssDbgNewSrc.index)
    {
      mssDbgNewSrc = mssDbg.new_src;
      angle = sysBfFilterAngles[sysBfVMicAngles[mssDbgNewSrc.index]];
      Log_write6(UIAEvt_intWithKey, angle, 1, mssDbgNewSrc.index, (IArg)"MSS-N: %d, G:%d", 1, mssDbgNewSrc.group);
    }

    /* Done with MSS */

#if (SYS_USE_DRC)
    /* Run DRC */
    t1 = Timestamp_get();
    inst_p      = sysContext.drcInst_p;     /* fetch the DRC instance pointer */
    frame_p     = txOutFrame1;              /* point to the MSS output frame buffer and use it as input */
    outframe_p  = txOutFrame2;              /* point to DRC output frame */
    err = drcProcess(inst_p, frame_p,       /* instance and input frame pointers */
		     outframe_p);           /* pointer to output buffer pointer */
    t2 = Timestamp_get();
    delta = t2-t1;
    txTaskProfile.drc.min = MIN(txTaskProfile.drc.min,delta);
    txTaskProfile.drc.max = MAX(txTaskProfile.drc.max,delta);
    txTaskProfile.drc.n++;
    txTaskProfile.drc.total += (float)delta;
    /* Done with DRC */
#else
    outframe_p = frame_p;
#endif

    /* Run VAU */
#if 0
    t1 = Timestamp_get();
    inst_p  = sysContext.vauInst_p;         /* fetch the VAU instance pointer */
    frame_p = txOutFrame;                   /* point to the Tx output frame buffer and use it as input */
    alert = (tint)vauProcess(inst_p, frame_p,   /* instance and input frame pointers */
                             &outframe_p,       /* pointer to output buffer pointer */
                             &vauFrameType);    /* Indicating speech/noise */
                                                /*  ISPHENC1_FTYPE_SPEECH or ISPHENC1_FTYPE_NODATA */
    t2 = Timestamp_get();
    delta = t2-t1;
    txTaskProfile.vau.min = MIN(txTaskProfile.vau.min,delta);
    txTaskProfile.vau.max = MAX(txTaskProfile.vau.max,delta);
    txTaskProfile.vau.n++;
    txTaskProfile.vau.total += (float)delta;

    if (vauFrameType != vauOldFrameType) {
      vauOldFrameType = vauFrameType;         /* Record new frame type */

      /* Trace source selection */
      /*    Write Args:
       *      arg2: (value) 2-(VAU Frame Type)  So, silence will be 0, speech will be 2
       *      arg3: (aux1)  2 - indicate VAU trace
       *      arg4: (aux2)  0
       */

      Log_write4(UIAEvt_intWithKey, 15*(ISPHENC1_FTYPE_NODATA-vauFrameType), 2, 0, (IArg)"VAU-FT");
    }
    if (alert != oldalert) {
      oldalert = alert;         /* Record new alert state */

      /* Trace source selection */
      /*    Write Args:
       *      arg2: (value) Alert status
       *      arg3: (aux1)  3 - indicate VAU alert trace
       *      arg4: (aux2)  0
       */

      Log_write4(UIAEvt_intWithKey, 20*alert, 3, 0, (IArg)"VAU-ALERT");
    }
#endif

    /*---------------------------------*/
    /* Save samples to the output file */
    /*---------------------------------*/

    err = filWrite(fid, SYS_FRAME_LENGTH, outframe_p);
    if (err != SYS_ERR_SUCCESS) {
      if (err == SYS_ERR_EOF) {
        if (!sysContext.eof) {
          sysContext.eof = TRUE;
          System_printf("taskTx: EOF Reached.\n");
          System_printf("\nTest Passed\n");
          System_flush();
        }
      }
      else {
        SYS_CHECK_ERROR(err);
      }
    } /* if */
  } /* loop */
} /* taskTx */

/* nothing past this point */

