/*
 * audio_evmInit.c
 *
 * This file contains Audio application related EVM (platform) specific routines
 * implementation.
 * This is part of the EVM Initialization library (evmInit) which contains pinmux
 * enabling routines for peripherals used by the application, and may contain
 * related device pre-driver initialization routines.
 *
 * Copyright (C) 2009-2017 Texas Instruments Incorporated - http://www.ti.com/
 *
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *    Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 *    Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the
 *    distribution.
 *
 *    Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 *  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 *  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 *  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 *  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 *  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 *  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 *  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 *  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 *  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
*/

/**
 *  \file   audio_evmInit.c
 *
 *  \brief  This file contains the board specific code for enabling the use of
 *          audio driver.
 *
 *  (C) Copyright 2009-2017, Texas Instruments, Inc
 *
 */
#include "stdint.h"
#include "stdio.h"
#include "string.h"
#include <stdbool.h>
#include <ti/csl/hw_types.h>
#include <ti/csl/csl_mcasp.h>
#include <ti/csl/csl_edma.h>
#include <ti/csl/src/ip/mcspi/V1/spi.h>
#include <ti/csl/src/ip/gpio/V0/gpio.h>
#include <ti/csl/src/ip/i2c/V0/i2c.h>
#ifdef _TMS320C6X
#include <ti/csl/arch/c67x/interrupt.h>
#else
#include <ti/csl/arch/arm9/interrupt.h>
#endif
#include <ti/csl/soc.h>
#include <ti/csl/csl_syscfg.h>
#include <ti/csl/csl_syscfgAux.h>
#include "mcasp_audiodc_bios.h"

#include <xdc/std.h>
#define SW_I2C
#ifndef SW_I2C
#include <ti/drv/i2c/I2c.h>
#endif

#include <Audio_evmInit.h>
#include <mcasp_cfg.h>
#include <ti/drv/mcasp/soc/mcasp_soc.h>
#include <ti/csl/cslr_device.h>
#include <ti/csl/csl_bootcfg.h>
#include <board.h>
#include <ti/csl/cslr_syscfg.h>

#include <ti/csl/hw_types.h>
#include <ti/drv/uart/UART_stdio.h>

#include "cmb.h"

extern Board_STATUS Board_init(Board_initCfg);

/*
 ** Configures McASP pin muxing
 */
void configMcaspPinMux(void)
{
    // Pinmux settings for for McASP1
    /*
     ** Clearing the bits in context and retaining the other bit values
     ** of PINMUX8 register.
     */
    hSysCfg->PINMUX8 &= (~(CSL_SYSCFG_PINMUX8_PINMUX8_23_20_MASK));

    /*
     ** Performing the actual Pin Multiplexing to select mandatory pins in
     ** PINMUX8  register.
     */
    hSysCfg->PINMUX8 |= PINMUX8_MCASP1_AXR10_ENABLE;

    // Pinmux settings for for McASP1
    /*
     ** Clearing the bits in context and retaining the other bit values
     ** of PINMUX11 register.
     */
    hSysCfg->PINMUX11 &= (~(CSL_SYSCFG_PINMUX11_PINMUX11_31_28_MASK
            | CSL_SYSCFG_PINMUX11_PINMUX11_27_24_MASK
            | CSL_SYSCFG_PINMUX11_PINMUX11_23_20_MASK));

    /*
     ** Performing the actual Pin Multiplexing to select mandatory pins in
     ** PINMUX11  register.
     */
    hSysCfg->PINMUX11 |= (PINMUX11_MCASP1_AFSX_ENABLE
            | PINMUX11_MCASP1_ACLKX_ENABLE |
            PINMUX11_MCASP1_AHCLKX_ENABLE);

    /*
     ** Clearing the bits in context and retaining the other bit values
     ** of PINMUX12 register.
     */
    hSysCfg->PINMUX12 &= (~(CSL_SYSCFG_PINMUX12_PINMUX12_19_16_MASK
            | CSL_SYSCFG_PINMUX12_PINMUX12_15_12_MASK
            | CSL_SYSCFG_PINMUX12_PINMUX12_11_8_MASK
            | CSL_SYSCFG_PINMUX12_PINMUX12_7_4_MASK
            | CSL_SYSCFG_PINMUX12_PINMUX12_3_0_MASK
			| CSL_SYSCFG_PINMUX12_PINMUX12_23_20_MASK
			| CSL_SYSCFG_PINMUX12_PINMUX12_27_24_MASK
			| CSL_SYSCFG_PINMUX12_PINMUX12_31_28_MASK));

    /*
     ** Performing the actual Pin Multiplexing to select mandatory pins in
     ** PINMUX12 register.
     */
    hSysCfg->PINMUX12 |= (PINMUX12_MCASP1_AXR0_ENABLE |
    		PINMUX12_MCASP1_AMUTE_ENABLE | PINMUX12_MCASP1_AFSR_ENABLE |
            PINMUX12_MCASP1_ACLKR_ENABLE | PINMUX12_MCASP1_AHCLKR_ENABLE |
			PINMUX12_MCASP1_AXR0_ENABLE | PINMUX12_MCASP1_AXR1_ENABLE |
			PINMUX12_MCASP1_AXR2_ENABLE | PINMUX12_MCASP1_AXR3_ENABLE);

    /*
     ** Clearing the bits in context and retaining the other bit values
     ** of PINMUX13 register.
     */
    hSysCfg->PINMUX13 &= (~(CSL_SYSCFG_PINMUX13_PINMUX13_7_4_MASK));

    /*
     ** Performing the actual Pin Multiplexing to select mandatory pins in
     ** PINMUX13 register.
     */
    hSysCfg->PINMUX13 |= (PINMUX13_MCASP1_AXR5_ENABLE);

    // Pinmux settings for enable AMUTE0
    /*
     ** Clearing the bits in context and retaining the other bit values
     ** of PINMUX10 register.
     */
    hSysCfg->PINMUX10 &= (~(CSL_SYSCFG_PINMUX10_PINMUX10_3_0_MASK));

    /*
     ** Performing the actual Pin Multiplexing to select mandatory pins in
     ** PINMUX10 register.
     */
    hSysCfg->PINMUX10 |= (PINMUX10_AMUTE0_ENABLE);
}

void configureAudio(void)
{
	Board_STATUS stat = BOARD_SOK;
	Board_initCfg arg = BOARD_INIT_PINMUX_CONFIG | BOARD_INIT_UART_STDIO;
	stat = Board_init(arg);

	configMcaspPinMux();

	cmb_AudioInit();

   return;
}

void configMcASP_SocHwInfo(void)
{
    /* Configure the interrupts for the McASP Instance MCASP_NUM */
    Mcasp_HwInfo hwInfo;

    Mcasp_socGetInitCfg(MCASP_NUM, &hwInfo);


/* Change anything if needed */
    Mcasp_socSetInitCfg(MCASP_NUM,&hwInfo);
}

/* ========================================================================== */
/*                              END OF FILE                                   */
/* ========================================================================== */
