#*******************************************************************************
#* FILE PURPOSE: Lower level makefile for Creating Component Libraries
#*******************************************************************************
#* FILE NAME: lnkr/ce66le/test_rel_ce66le.xe66.mk
#*
#* DESCRIPTION: Defines Source Files, Compilers flags and build rules
#*
#*
#* This is an auto-generated file		  
#*******************************************************************************
#

#
# Macro definitions referenced below
#
empty =
space =$(empty) $(empty)
CC = "$(C66X_GEN_INSTALL_DIR)/bin/"cl6x -c -mv6600 --abi=eabi
AC = "$(C66X_GEN_INSTALL_DIR)/bin/"cl6x -c -mv6600 --abi=eabi
ARIN = "$(C66X_GEN_INSTALL_DIR)/bin/"ar6x rq
LD = "$(C66X_GEN_INSTALL_DIR)/bin/"lnk6x --abi=eabi 
CGINCS = $(strip $(subst $(space),\$(space),$(C66X_GEN_INSTALL_DIR)/include))
RTSLIB = -l "$(C66X_GEN_INSTALL_DIR)/lib/libc.a"
INCS = -I. -I$(strip $(subst ;, -I,$(subst $(space),\$(space),$(subst \,/,$(INCDIR)))))
OBJEXT = oe66
AOBJEXT = se66
INTERNALDEFS =  -Dti_targets_elf_C66 -Dxdc_target_types__=ti/targets/std.h -eo.$(OBJEXT) -ea.$(AOBJEXT) -fr=$(@D) -fs=$(@D) -ppa -ppd=$@.dep 
INTERNALLINKDEFS =  -o $@ -m $@.map
OBJDIR = ./package/cfg/lnkr/ce66le/test_rel_ce66le

#List the commonCsrc Files
COMMONCSRCC= \
	src/vpesim.c\
	src/nrsim.c\
	src/nrbufs.c\
	src/vpesim_setup.c

# FLAGS for the commonCsrc Files
COMMONCSRCCFLAGS =  -c -k -q --mem_model:data=far -al -pds1111 -pds827 -pds824 -pds837 -pds1037 -pds195 -pdsw225 -pdsw994 -pdsw262 -pds77 -pden -pds232 -mw -os -g -mi10000 -as -ss -o3 --optimize_with_debug -ms3

# Make Rule for the commonCsrc Files
COMMONCSRCCOBJS = $(patsubst %.c, $(OBJDIR)/%.$(OBJEXT), $(COMMONCSRCC))

$(COMMONCSRCCOBJS): $(OBJDIR)/%.$(OBJEXT): %.c
	-@echo cle66 $< ...
	if [ ! -d $(@D) ]; then $(MKDIR) $(@D) ; fi;
	-$(RM) $@.dep
	$(CC) $(COMMONCSRCCFLAGS) $(INTERNALDEFS) $(INCS) -I$(CGINCS) -fc $< 
	-@$(CP) $@.dep $@.pp; \
			$(SED) -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
				-e '/^$$/ d' -e 's/$$/ :/' < $@.pp >> $@.dep; \
			$(RM) $@.pp 

#Create Empty rule for dependency
$(COMMONCSRCCOBJS):lnkr\ce66le\test_rel_ce66le.xe66.mk
lnkr\ce66le\test_rel_ce66le.xe66.mk:

#Include Depedency for commonCsrc Files
ifneq (clean,$(MAKECMDGOALS))
 -include $(COMMONCSRCCOBJS:%.$(OBJEXT)=%.$(OBJEXT).dep)
endif



lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/sdk/lib/sdk_c.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/vpe/lib/vpe_cm.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/vpe/lib/vpe_c.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/vpe/lib/vpe_a.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/util/lib/util_cm.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/util/lib/util_c.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  $(VOLIB_INSTALL_DIR)/ti/mas/util/lib/util_a.ae66
lnkr/ce66le/test_rel_ce66le.xe66 :  lnkr/ce66le/test_rel_ce66le.cmd
lnkr/ce66le/test_rel_ce66le.xe66 :  $(COMMONCSRCCOBJS)
	@echo lnke66 $@ ...
	$(LD)  $(COMMONCSRCCOBJS) lnkr/ce66le/test_rel_ce66le.cmd $(INTERNALLINKDEFS) $(RTSLIB)
