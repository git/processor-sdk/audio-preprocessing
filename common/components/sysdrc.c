/*=================================================================
 *  sysdrc.c: DRC creation routines.
 *=================================================================*/

#include <xdc/std.h>
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/System.h>

#include <types.h>
#include <ti/mas/fract/fract.h>
#include <ti/mas/util/ecomem.h>

#include <ti/mas/aer/drc.h>

#include "sys.h"

drcSizeConfig_t drcSizeConfig = {
  drc_SAMP_RATE_16K,          /* sampling rate */
  16*drc_SAMP_RATE_16K        /* 2ms maximum forward looking limiter delay in samples */
};

drcOpenConfig_t drcOpenConfig = {   /* for drcOpen */
  ((1<<drc_CFGBITS_FBAND) |   /* use full-band operation */
   (1<<drc_CFGBITS_LIM)   |   /* use limiter */
   (1<<drc_CFGBITS_VAD)),     /* update gains only during speech */
  drc_SAMP_RATE_16K           /* sampling rate */
};

static void sysDbgInfo (void *handle, tuint par1, tuint par2, tuint par3, tuint *ptr)
{
  return;
} /* dummy sysDbgInfo */

/*=================================================================
 *  void sysDrcCreate(void)     Create DRC
 *=================================================================*/
void sysDrcCreate(void)
{
  int   err;
  tint  nbufs;
  const ecomemBuffer_t  *bufs;
  drcNewConfig_t drcNewConfig;
  drcControl_t   drcCtl;

  drcCreateConfig_t drcCreateConfig = {sysDbgInfo}; /* pointer to a dummy debug info (not used in DRC) */

  System_printf("...Initializing DRC\n");
  System_flush();

  /* Create DRC Context */
  err = drcCreate (&drcCreateConfig);
  if (err != drc_NOERR) {
    System_printf("*** drcCreate() error: %d\n", err);
    BIOS_exit(0);
  }

  /* Configure DRC */
  err = drcGetSizes(&nbufs, &bufs, &drcSizeConfig);
  if (err != drc_NOERR) {
    System_printf("*** drcGetSizes error: %d\n", err);
    BIOS_exit(0);
  }

  /* Allocate memory for beamformer */
  if (nbufs > SYS_COMP_MAXBUFS) {
    System_printf("*** not enough buffer descriptors");
    BIOS_exit(0);
  }

  drcNewConfig.sizeCfg = drcSizeConfig;
  err = sysHeapAllocAll(nbufs, sysCompBufs, (const void*)bufs);
  SYS_CHECK_ERROR(err);
  
  /* Give memory to DRC */
  drcNewConfig.handle = (void*)0;     /* Indicate instance #0 */
  sysContext.drcInst_p = NULL;
  err = drcNew(&sysContext.drcInst_p, nbufs, sysCompBufs, &drcNewConfig);
  if (err != drc_NOERR) {
    System_printf("*** drcNew() error: %d\n", err);
    BIOS_exit(0);
  }

  /* Open DRC for business */
  err = drcOpen(sysContext.drcInst_p,&drcOpenConfig);
  if (err != drc_NOERR) {
    System_printf("*** drcOpen() error: %d\n", err);
      BIOS_exit(0);
  }
  /* At this point DRC is open, but DISABLED! */
  /* We need to do a few additional configurations through drcControl() */

  /* Set the full band companding curve */
  drcCtl.ctl_code = drc_CTL_SET_FBAND;
  drcCtl.u.band.valid_bitfield    = (1u<<drc_BAND_CFG_VALID_CURVE_PARAM_BIT);
  drcCtl.u.band.curve.exp_knee    = sysContext.drc_exp_knee;    /* in dBm0! */
  drcCtl.u.band.curve.ratios      = frctAdjustQ(2,0,4) | (frctAdjustQ(2,0,4) << 8);
  drcCtl.u.band.curve.max_amp     = sysContext.drc_max_amp;     /* in dB */
  drcCtl.u.band.curve.com_knee    = -15;    /* -15dBm0 */
  drcCtl.u.band.curve.energy_lim  = -3;     /* -3dBm0 */
  err = drcControl (sysContext.drcInst_p, &drcCtl);
  if (err != drc_NOERR) {
    System_printf("*** drcControl() config full band error: %d\n", err);
    BIOS_exit(0);
  }

  /* Set the limiter configuration */
  drcCtl.ctl_code = drc_CTL_SET_LIM;
  drcCtl.u.limiter.valid_bitfield = (1u<<drc_LIM_CFG_VALID_THRESH_BIT) |
                                    (1u<<drc_LIM_CFG_VALID_DELAY_BIT);
  drcCtl.u.limiter.thresh_dBm = -3;       /* -3dBm0 */
  drcCtl.u.limiter.delay_len  = (8*drc_SAMP_RATE_16K);    /* just the current value! */
  err = drcControl (sysContext.drcInst_p, &drcCtl);
  if (err != drc_NOERR) {
    System_printf("*** drcControl() config limiter error: %d\n", err);
    BIOS_exit(0);
  }

  /* Turn DRC on */
  if (sysContext.drc_enable) {
    drcCtl.ctl_code = drc_CTL_DRC_ON;
    err = drcControl (sysContext.drcInst_p, &drcCtl);
    if (err != drc_NOERR) {
      System_printf("*** drcControl() enable error: %d\n", err);
      BIOS_exit(0);
    }
  }

  System_printf("Done with DRC\n");
  System_flush();

} /* sysDrcCreate */

/* nothing past this point */

